package server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import protocol.Protocol;
import view.ClientTUIView;

public class Server implements Protocol {
	
	private List<ClientHandler> clients;
	private List<ClientHandler> lobby;
	private List<ServerGame> games;
	private Map<Integer, List<ClientHandler>> queue;
	private int port;
	private ServerSocket ssock;

	public static void main(String[] args) {
		Server server = new Server();
		server.run();
	}
	
	/**
	 * Creates a server by invoking the method startUp().
	 */
	public Server() {
		startUp();
		this.clients = new ArrayList<ClientHandler>();
		this.games = new ArrayList<ServerGame>();
		this.lobby = new ArrayList<ClientHandler>();
		this.queue = new HashMap<>();
		
	}
	
	/**
	 * Asks for a port number to use for the server. 
	 * When the port is already in use a error message will be printed.
	 * If everything went right a ServerSocket with given port number will be created.
	 */
	public void startUp() {
		int tempPort = 1337;
		String message = null;
		boolean validPort = false;
		while (!validPort) {
			message = readString("Enter the port you would like to use: ");
			try {
				tempPort = Integer.parseInt(message);
				if (tempPort >= 1 && tempPort <= 65535) {
					this.port = tempPort;
					validPort = true;
				} else {
					print("Invalid port number, enter a port between 1 ~ 65535");
				}
			} catch (NumberFormatException e) {
				print("Make sure you enter a number.");
			}
		}
		try {
			ssock = new ServerSocket(this.port);
		} catch (IOException e) {
			print("The port you want to use is already in use, try a different port");
			this.startUp();
		}
		print("Server connection succesfully established. Port in use: " + tempPort);
	} 
	
	/**
	 * Adds a client to the queue, removes this client from the lobby 
	 * and the method checkQueue will be called.
	 * @param client: The client who will be added to the queue (who sent the message READY).
	 * @param players: The amount of players you want to play a game with.
	 */
	public void addToQueue(ClientHandler client, int players) {
		List<ClientHandler> addQueue = null;
		if (!this.queue.containsKey(players)) {
			addQueue = new ArrayList<>();
			this.queue.put(players, addQueue);
		} else {
			addQueue = this.queue.get(players);
		}
		addQueue.add(client);
		this.lobby.remove(client);
		ClientTUIView.print(client.getClientName() + " has been added to the queue for " + 
				players + " player games");
		this.checkQueue(players);
	}
	
	/**
	 * The run method of the class Server. 
	 * Creates a Socket with a ServerSocket which waits for a connection to be made.
	 * Afterwards a ClientHandler is made with this Socket and will be started.
	 */
	public void run() {
		try {
			while (true) {
				Socket sock = ssock.accept();
				ClientHandler handler = new ClientHandler(this, sock);
				addHandler(handler);
				print("Handler added");
				handler.start();
			}
		} catch (IOException e) {
			startUp();
		}
	}
	
	/**
	 * A method which will make printing messages easier.
	 * @param message: the message you want to print.
	 */
	public void print(String message) {
		System.out.println(message);
	}
	
	/**
	 * Broadcasts a message to all clients on the server.
	 * @param msg: the message you want to broadcast
	 */
	public void broadcast(String msg) {
		print(msg);
		for (ClientHandler handler : clients) {
			handler.sendMessage(msg);
		}
	}
	
	/**
	 * Broadcasts a message to all clients in a certain game.
	 * @param msg: the message you want to broadcast.
	 * @param handlers: the clients who are in a certain game you want to broadcast a message to.
	 */
	public synchronized void broadcast(String msg, List<ClientHandler> handlers) {
		for (ClientHandler h: handlers) {
			h.sendMessage(msg);
		}
	}
	
	/**
	 * Broadcasts a message to a certain client.
	 * @param msg: the message you want to broadcast.
	 * @param handler: the client you want to broadcast a message to.
	 */
	public synchronized void broadcast(String msg, ClientHandler handler) {
		print(msg);
		handler.sendMessage(msg);
	}
	
	/**
	 * Removes a handler from the list of clients.
	 * @param handler the handler you want to remove.
	 */
	public void removeHandler(ClientHandler handler) {
		System.out.println(handler.getClientName() + "is being removed from the client list");
		clients.remove(handler);
	}
	
	public boolean doesUserNameExist(String name) {
		boolean result = false;
		for (int i = 0; i < this.clients.size() && !result; i++) {
			ClientHandler client = this.clients.get(i);
			if (client.getClientName().equals(name)) {
				result = true;
			}
		}
		return result;
	}
	
	public boolean isValidUserName(String name) {
		Pattern p = Pattern.compile("[^a-zA-Z]");
		Matcher specialCharacter = p.matcher(name);
		boolean length = name.length() <= 26;
		boolean hasSpecialCharacter = specialCharacter.find();
		return length && !hasSpecialCharacter;
	}
	
	/**
	 * Return the real name of a ClientHandler.
	 * @param name 
	 * Returns null if there is no ClientHandler with the given name.
	 */
	public ClientHandler getHandlerByName(String name) {
    	for (ClientHandler c : clients) {
    		if (c.getClientName() != null) {
    			if (c.getClientName().equals(name)) {
        			return c;
        		}
    		}
    	}
    	return null;
    }
	
	/**
	 * Removes a servergame from the lists of servergames.
	 * Removes the ClientHandlers who were in that game.
	 * Afterwards it will put the removed ClientHandlers in the lobby.
	 * @param serverGame the game you want to remove.
	 */
	public void removeGame(ServerGame serverGame) {
		for (ClientHandler c : serverGame.getHandlers()) {
			c.removeGame(serverGame);
			this.addToLobby(c);
		}
		games.remove(serverGame);
		ClientTUIView.print("The game has been removed");
	}
	
	/**
	 * Adds a handler to the list of clients.
	 * @param client: the client you want to add.
	 */
	public void addHandler(ClientHandler client) {
		this.clients.add(client);
	}
	
	/**
	 * Creates a new ServerGame with the given gamePlayers as input.
	 * Adds the game to the list with games.
	 * @param gamePlayers
	 */
	public void createGame(List<ClientHandler> gamePlayers) {
		ServerGame game = new ServerGame(gamePlayers);
		for (ClientHandler addGameClient : gamePlayers) {
			addGameClient.setGame(game);
		}
		games.add(game);
		sendStartGame(gamePlayers);
	}
	
	/**
	 * Sends the STARTGAME command to all the clients in a certain game.
	 * Example: game started with player1 player2.
	 * @param gamePlayers
	 */
	public void sendStartGame(List<ClientHandler> gamePlayers) {
		String playerNames = "";
		for (int k = 0; k < gamePlayers.size(); k++) {
			ClientHandler player = gamePlayers.get(k);
			if (k < gamePlayers.size() - 1) {
				playerNames = playerNames + player.getClientName() + " ";
			} else {
				playerNames = playerNames + player.getClientName();
			}
		}
		this.broadcast(String.format(STARTGAME, playerNames), gamePlayers);
	}
	
	/**
	 * Starts a servergame if there are enough players ready to play a game. 
	 * When the players are added to the twoPlayerQueue, they will be removed from the queue.
	 * There will be no game starting if there is only 1 player in the queue. 
	 */
	public void checkQueue(int gameSize) {
		ClientTUIView.print("The queue is being checked");
		List<ClientHandler> checkQueue = this.queue.get(gameSize);
		if (checkQueue.size() >= gameSize) {
			for (int i = 0; i < (checkQueue.size() / gameSize); i++) {			
				List<ClientHandler> gamePlayers = new ArrayList<>();
				for (int j = 0; j < gameSize; j++) {
					ClientHandler client = checkQueue.get(j);
					gamePlayers.add(client);
				}
				createGame(gamePlayers);
				checkQueue.removeAll(gamePlayers);
			}
		}		
	}		
	
	/**
	 * Adds a client to the lobby.
	 * @param client: the client you want to add to the lobby.
	 */
	public void addToLobby(ClientHandler client) {
		lobby.add(client);
		ClientTUIView.print(client.getClientName() + " has been added to the lobby.");
	}
	
	/**
	 * A method which creates a BufferedReader which you can use.
	 * @param message: the message you want to read.
	 */
	public static String readString(String message) {
		System.out.print(message);
		String input = null;
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					System.in));
			input = in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return (input == null) ? "" : input;
	}	
}