package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Scanner;

import exceptions.InvalidMoveException;
import protocol.Protocol;
import view.ClientTUIView;

public class ClientHandler extends Thread implements Protocol {
	
	private Server server;
	private Socket clientSock;
	private BufferedReader in;
    private BufferedWriter out;
    private boolean isRunning;
    private String clientName = "";
    private ServerGame serverGame;
    private int players = 2;
    
    private static final String JOIN = "Join";
    private static final String READY = "Ready";
    private static final String MOVE = "Move";
    private static final String LEAVE = "Leave";
    private static final String DISCONNECT = "Disconnect";

    /**
     * The constructer of ClientHandler.
     * @param server: the server of the ClientHandler.
     * @param sock: the Sock of the client.
     */
	public ClientHandler(Server server, Socket sock) {
		this.server = server;
		this.clientSock = sock;
		try {
			this.in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			this.out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));
		} catch (IOException e) {
			this.shutDown();
		}
		this.isRunning = true;
	}
	
	/**
	 * The run method of ClientHandler.
	 */
	public void run() {
    	try {
			while (isRunning) {
				if (in.ready()) {
					String command = in.readLine();
					this.execute(command);
				}
			}
		} catch (IOException e) {
			this.shutDown();
		} 	
    }
	
	/**
	 * Sends messages to the out of ClientHandler.
	 * @param msg
	 */
	public void sendMessage(String msg) {
		try {
			System.out.println(msg);
			out.write(msg + "\n");
			out.flush();
		} catch (IOException e) {
			System.out.println("Sending message: " + msg + " failed!");
			this.shutDown();
		}
	}
	
	/**
	 * Shuts down the socket connection and closes the in and out of the ClientHandler.
	 */
	public void shutDown() {
		try {
    		if (this.serverGame != null) {
    			server.broadcast(String.format(CONNECTIONLOST, this.clientName), 
    					serverGame.getHandlers());
    			server.removeGame(serverGame);
    		}
			out.close();
			in.close();
			clientSock.close();
			server.removeHandler(this);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
	
	/**
	 * Executes commands given to this method by the client.
	 * When one of the 5 messages is send from client to server 
	 * the corresponding methods will be called and executed.
	 * @param message the message you want to execute.
	 */
	private void execute(String line) {
		Scanner inScanner = new Scanner(line);
		if (inScanner.hasNext()) {
			String cmd = inScanner.next();
			switch (cmd) {
				case JOIN:
					join(inScanner);
					break;
				case READY:
					ready(inScanner);
					break;
				case LEAVE:
					leave();
					break;
				case MOVE:
					move(inScanner);
					break;
				case DISCONNECT:
					disconnect();
					break;
				default:
					ClientTUIView.printError("No command reveived");
			}
		}
		inScanner.close();
	}
	
	/**
	 * Joins the client with a name, which is not in use, to the lobby.
	 * If there is no name given the IP + ClientHandler will be set as the name of the client.
	 * @param scanner
	 */
	public void join(Scanner scanner) {
		if (scanner.hasNext()) {
			String tempClientName = scanner.next();
			if (server.doesUserNameExist(tempClientName)) {
				this.sendMessage(String.format(ERROR, "sorry this username already exist"));
			} else if (!server.isValidUserName(tempClientName)) {
				this.sendMessage(String.format(ERROR, "Please choose a name with only letters"));
			} else {
				this.clientName = tempClientName;
				server.addToLobby(this);
			}			
		} else {
			String tempClientName = this.clientSock.getInetAddress().toString() + " ClientHandler";
			if (server.doesUserNameExist(tempClientName)) {
				this.sendMessage(String.format(ERROR, "sorry this username already exist"));
			} else {
				this.clientName = tempClientName;
				server.addToLobby(this);
			}
		}		
	}
	
	/**
	 * Puts the client in a queue for the given player amount.
	 * @param scanner
	 */
	public void ready(Scanner scanner) {
		if (scanner.hasNextInt()) {
			this.players = scanner.nextInt();
		}
		server.addToQueue(this, this.players);
	}
	
	/**
	 * Removes the ServerGame if this client is in a game.
	 */
	public void leave() {
		if (this.serverGame != null) {
			server.broadcast(String.format(CONNECTIONLOST, this.clientName));
			server.removeGame(serverGame);
		} else {
			server.broadcast(String.format(ERROR, "can't send leave command if not in game"), this);
		}
	}
	
	/**
	 * Receives the x and y of a move and sets this move in the server board.
	 * Afterwards sends a NOTIFYMOVE command with this x and y.
	 * If the game is over the result will be printed. 
	 * @param scanner
	 */
	public void move(Scanner scanner) {
		int x = 0;
		int y = 0;
		if (serverGame.getCurrentHandler().equals(this)) {
			if (scanner.hasNextInt()) {
				x = scanner.nextInt();
			} else {
				server.broadcast(String.format(ERROR, this.clientName
						+ " no x coordinate received."), this);
			}
			if (scanner.hasNextInt()) {
				y = scanner.nextInt();
			} else {
				server.broadcast(String.format(ERROR, this.clientName 
						+ " no y coordinate received."), this);
			}
			try {
				this.serverGame.setMove(x, y);
				System.out.println("The game result is: " + this.serverGame.getGame().gameOver());
				System.out.println("The server has the following board \n" + 
						this.serverGame.getBoard().toString());
				if (this.serverGame.getGame().gameOver()) {
					server.broadcast(String.format(GAMEOVER, serverGame.getGame().getResult()), 
							serverGame.getHandlers());
					server.removeGame(serverGame);
				} else {
					server.broadcast(String.format(NOTIFYMOVE, x, y), serverGame.getHandlers());
				}				
			} catch (InvalidMoveException e) {
				server.broadcast(String.format(ERROR, this.clientName + 
						" made an invalid move :("), serverGame.getHandlers());
			}
			
		} else {
			server.broadcast(String.format(ERROR, this.clientName + 
					"tried to go before his turn :("), serverGame.getHandlers());
		}
	}
	
	public void disconnect() {
		this.shutDown();
	}
	
	/**
	 * Removes the game.
	 * @param game: the game you want to remove.
	 */
	public void removeGame(ServerGame game) {
    	this.serverGame = null;
    }
	
	/**
	 * @return the clientname.
	 */
	public String getClientName() {
		return this.clientName;
	}
	
	/**
	 * @return a servergame.
	 */
	public ServerGame getGame() {
		return this.serverGame;
	}
	
	/**
	 * @param game: sets a game as servergame.
	 */
	public void setGame(ServerGame game) {
		this.serverGame = game;
	}
}
