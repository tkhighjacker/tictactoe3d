package server;

import java.util.ArrayList;
import java.util.List;

import controller.Game;
import controller.NetworkPlayer;
import controller.Player;
import exceptions.InvalidMoveException;
import model.Board;
import protocol.Protocol;

/**
 * Class for maintaining the Tic Tac Toe game. Lab assignment Module 2
 * 
 * @author Theo Ruys en Arend Rensink
 * @version $Revision: 1.4 $
 */
public class ServerGame extends Thread implements Protocol {



	// -- Instance variables -----------------------------------------

	/*@
       private invariant board != null;
	 */
	/**
	 * The board.
	 */
	private Game game;
	private Board board;

	/*@
    private invariant players.length == NUMBER_PLAYERS;
    private invariant (\forall int i; 0 <= i && i < NUMBER_PLAYERS; players[i] != null); 
	 */
	/**
	 * The players of the game.
	 */
	private List<Player> players = new ArrayList<>();

	/*@
       private invariant players.length == NUMBER_PLAYERS;
       private invariant (\forall int i; 0 <= i && i < NUMBER_PLAYERS; players[i] != null); 
	 */
	/**
	 * The players of the game.
	 */
	private List<ClientHandler> handlers;
	/*@
       private invariant 0 <= current  && current < NUMBER_PLAYERS;
	 */
	/**
	 * Index of the current player.
	 */
	private int current;

	// -- Constructors -----------------------------------------------

	/*@
      requires players != null;
      requires server != null;
	 */
	/**
	 * The constructor of ServerGame.
	 * @param players: the ClientHandlers of a ServerGame.
	 */
	public ServerGame(List<ClientHandler> handlers) {
		this.handlers = handlers;
		for (int i = 0; i < handlers.size(); i++) {
			String handlerName = handlers.get(i).getClientName();
			players.add(new NetworkPlayer(handlerName));			
		}
		this.game = new Game(players);
		this.board = game.getBoard();
	}

	// -- Commands ---------------------------------------------------
	
	/**
	 * Sets a move on the board of a ServerGame.
	 * Afterwards determines possible moves for the next player and sets this move.
	 * @param x: the x of the move.
	 * @param y: the y of the move.
	 * @throws InvalidMoveException throws a InvalidMoveException if the move is not possible.
	 */
	public void setMove(int x, int y) throws InvalidMoveException {
		this.getCurrentPlayer().setMove(x, y);
		this.getCurrentPlayer().determineMove(board);
		this.board.setField(x, y, board.getZ(x, y), getCurrentPlayer().getAbbreviation());
		this.incrementCurrent();
	}
	
	/**
	 * Increments the current by 1 if a players turn is over.
	 * 
	 */
	public void incrementCurrent() {
		this.current = (this.current + 1) % players.size();
	}
	
	/**
	 * @return the game.
	 */
	public Game getGame() {
		return this.game;
	}
	
	/**
	 * @return the board of a game.
	 */
	public Board getBoard() {
		return this.board;
	}

	/**
	 * @return the current.
	 */
	public int getCurrent() {
		return this.current;
	}

	/**
	 * @return the current ClientHandler.
	 */
	public ClientHandler getCurrentHandler() {
		return this.handlers.get(this.current);
	}

	/**
	 * @return all ClientHandlers.
	 */
	public List<ClientHandler> getHandlers() {
		return this.handlers;
	}

	/**
	 * @return the current player casted to a NetworkPlayer.
	 */
	public NetworkPlayer getCurrentPlayer() {
		return (NetworkPlayer) this.players.get(current);
	}

	/**
	 * @return all the players.
	 */
	public List<Player> getPlayers() {
		return this.players;
	}
}
