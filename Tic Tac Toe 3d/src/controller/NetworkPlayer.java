package controller;

import exceptions.InvalidMoveException;
import model.Board;

public class NetworkPlayer extends Player {
	
	public NetworkPlayer(String name) {
		super(name);
	}

	private int moveX;
	private int moveY; 

	/**
     * Determines the field for the next move.
     * 
     * @param board
     *            the current game board
     * @return the player's choice
     * @throws InvalidMoveException when an invalid move is made
     */
	public int[] determineMove(Board board) throws InvalidMoveException {
		boolean isValid = false;
		int[] result = new int[Board.DIM];
		int moveZ = board.getZ(this.moveX, this.moveY);
		isValid = board.isField(this.moveX, this.moveY, moveZ) 
				&& board.isEmptyField(this.moveX, this.moveY, moveZ) 
				&& board.isValidMove(this.moveX, this.moveY, moveZ);
		if (isValid) {
			result[0] = this.moveX;
			result[1] = this.moveY;
			result[2] = moveZ;
		} else {
			throw new InvalidMoveException(this.moveX, this.moveY);
		}
		return result;
	}
	
	/**
	 * Sets a move with the given x and y.
	 * @param x: the x of the move.
	 * @param y: the y of the move.
	 */
	public void setMove(int x, int y) {
		this.setMoveX(x);
		this.setMoveY(y);
	}

	/** 
	 * Sets the x.
	 * @param x: the x of the move.
	 */
	public void setMoveX(int x) {
		this.moveX = x;
	}
	
	/**
	 * Sets the y.
	 * @param y: the y of the move.
	 */
	public void setMoveY(int y) {
		this.moveY = y;
	}

}
