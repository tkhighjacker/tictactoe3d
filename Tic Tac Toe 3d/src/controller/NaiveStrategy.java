package controller;

import java.util.*;

import exceptions.InvalidMoveException;
import model.Board;

public class NaiveStrategy implements Strategy {
	
	private String name;

	/** 
	 * Constructor of NaiveStrategy.
	 */
	public NaiveStrategy() {
		this.name = "Naive";
	}

	/**
	 * Returns the name.
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/**
     * Determines the field for the next move.
     * 
     * @param board
     *            the current game board
     * @return the player's choice
     * @throws InvalidMoveException when an invalid move is made
     */
	@Override
	public int[] determineMove(Board b, String m) {
		int[] result = new int[3];
		List<int[]> emptyFields = SmartStrategy.getEmptyFields(b);	
		Iterator<int[]> it = emptyFields.iterator();
		int field = (int) (Math.random() * emptyFields.size());
		for (int i = 0; i < field && it.hasNext() && emptyFields.size() != 1; i++) {
			result = it.next();
		}
		if (emptyFields.size() == 1) {
			result = it.next();
		}
		return result;
	}

}
