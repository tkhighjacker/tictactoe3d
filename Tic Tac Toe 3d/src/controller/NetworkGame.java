package controller;

import model.Board;

import java.util.ArrayList;
import java.util.List;

public class NetworkGame {
	
	private Board clientBoard = new Board();
	private List<NetworkPlayer> players = new ArrayList<>();
	private int current;
	
	/**
	 * The constructer of NetworkGame.
	 */
	public NetworkGame() {
		this.current = 0;
	}
	
	/**
	 * Adds a new NetworkPlayer to the player list.
	 * @param name the name of the player.
	 */
	public void addNewClientPlayer(String name) {
		players.add(new NetworkPlayer(name));
	}
	
	/**
	 * @return the clientBoard.
	 */
	public Board getBoard() {
		return clientBoard;
	}
	
	/**
	 * @return the current player.
	 */
	public NetworkPlayer getCurrentPlayer() {
		return this.players.get(current);
	}
	
	/**
	 * @return all the NetworkPlayers.
	 */
	public List<NetworkPlayer> getPlayers() {
		return this.players;
	}
	
	/**
	 * Increments the current by 1 if a players turn is over.
	 */
	public void incrementCurrent() {
		this.current = (this.current + 1) % players.size();
	}
	
	/**
	 * @return the abbreviation of the current player.
	 */
	public String getCurrentPlayerAbbreviation() {
		return this.players.get(current).getAbbreviation();
	}
	
	/**
	 * Puts a mark on the clientboard.
	 * Increments current afterwards.
	 * @param x: the x of the made move.
	 * @param y: the y of the made move.
	 */
	public void putMark(int x, int y) {		
		this.clientBoard.setField(x, y, clientBoard.getZ(x, y), getCurrentPlayerAbbreviation());
		this.incrementCurrent();
	}
	
	/**
	 * @return the player names of the list of players.
	 */
	public String getPlayerNames() {
		String result = "";
		for (int i = 0; i < players.size(); i++) {
			String name = players.get(i).getName();
			if (i < players.size() - 1) {
				result = result + name + " and ";
			} else {
				result = result + name;
			}
		}
		return result;
	}
}
