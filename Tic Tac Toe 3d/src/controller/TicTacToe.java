package controller;

import java.io.FilterInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import exceptions.InvalidMoveException;

/**
 * Executable class for the game Tic Tac Toe. The game can be played against the
 * computer. Lab assignment Module 2
 * 
 * @author Theo Ruys
 * @version $Revision: 1.4 $
 */
public class TicTacToe {
	
	/**
     * Wraps an input stream to prevent it from being closed.
     */
    private static class UncloseableInputStream extends FilterInputStream {

        /**
         * Creates a wrapper around {@link System.in}.
         */
        UncloseableInputStream() {
            this(System.in);
        }

        /**
         * Creates a wrapper around an arbitrary {@link InputStream}.
         * @param stream The stream to wrap.
         */
        UncloseableInputStream(InputStream stream) {
            super(stream);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void close() throws IOException {
            // Don't do anything
        }
    }
	
	
    public static void main(String[] args) {
    	System.setIn(new UncloseableInputStream());
    	Game game;
    	List<Player> players = new ArrayList<>();
    	int sPlayers = 0;
    	int nPlayers = 0;
    	int hPlayers = 0;
   		for (String s : args) {
   			if (s.equals("-N")) {
   				Player naive = null;
   				if (nPlayers == 0) {
   					naive = new ComputerPlayer(s, new NaiveStrategy());
   				} else {
   					naive = new ComputerPlayer(nPlayers + s, new NaiveStrategy());
   				}
   				players.add(naive);
   				nPlayers++;				
   			} else if (s.equals("-S")) {
   				Player smart = null;
   				if (sPlayers == 0) {
   					smart = new ComputerPlayer(s, new SmartStrategy());
   				} else {
   					smart = new ComputerPlayer(sPlayers + s, new SmartStrategy());
   				}
   				players.add(smart); 
   				sPlayers++;
   			} else {
   				boolean found = false;
   				Player human = null;
   				if (hPlayers == 0) {
   					human = new NormalPlayer(s);
   				} else {
   					for (int i = 0; i < players.size() && !found; i++) {
   	   					if (players.get(i).getName().equals(s)) {
   	   						found = true;
   	   					}
   	   				}
   					human = new NormalPlayer((hPlayers - 1) + s);
   				}
   				players.add(human);
   				hPlayers++;
   			}
   		}
   		game = new Game(players);
   		try {
			game.start();
		} catch (InvalidMoveException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	 
    }
}
