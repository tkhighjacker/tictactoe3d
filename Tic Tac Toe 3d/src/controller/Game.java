package controller;

import java.util.List;
import java.util.Scanner;

import exceptions.InvalidMoveException;
import model.Board;
import protocol.Protocol;
import view.ClientTUIView;

/**
 * Class for maintaining the Tic Tac Toe game. Lab assignment Module 2
 * 
 * @author Theo Ruys en Arend Rensink
 * @version $Revision: 1.4 $
 */
public class Game {
	
	

    // -- Instance variables -----------------------------------------

    private int numberPlayers;

    /*@
       private invariant board != null;
     */
    /**
     * The board.
     */
    private Board board;

    /*@
       private invariant players.length == NUMBER_PLAYERS;
       private invariant (\forall int i; 0 <= i && i < NUMBER_PLAYERS; players[i] != null); 
     */
    /**
     * The 2 players of the game.
     */
    private List<Player> players;

    /*@
       private invariant 0 <= current  && current < NUMBER_PLAYERS;
     */
    /**
     * Index of the current player.
     */
    private int current;

    // -- Constructors -----------------------------------------------

    /*@
      requires s0 != null;
      requires s1 != null;
     */
    /**
     * Creates a new Game object.
     * 
     * @param s0
     *            the first player
     * @param s1
     *            the second player
     */
    public Game(List<Player> players) {
        this.board = new Board();
        this.players = players;
        this.current = 0;
        this.numberPlayers = players.size();
    }

    // -- Commands ---------------------------------------------------
    
    /**
     * Returns true if the game is over. The game is over when there is a winner
     * or the whole student is full.
     *
     * @return true if the game is over
     */
    //@ ensures \result == this.isFull() || this.hasWinner();
    /*@pure*/
    public boolean gameOver() {
        return board.isFull() || this.hasWinner();
    }

    /**
     * Returns true if the game has a winner. This is the case when one of the
     * marks controls at least one row or diagonal.
     *
     * @return true if the student has a winner.
     */
    //@ ensures \result == isWinner(Mark.XX) | \result == isWinner(Mark.OO);
    /*@pure*/
    public boolean hasWinner() {
    	boolean result = false;
    	for (int i = 0; i < players.size() && !result; i++) {
    		result = board.isWinner(players.get(i).getAbbreviation());
    	}
        return result;
    }
    
    /**
     * @return the winner of a game.
     */
    public Player getWinnner() {
    	Player result = null;
    	for (Player player : players) {
    		if (board.isWinner(player.getAbbreviation())) {
    			result = player;
    		}
    	}
    	return result;
    }
    
    /**
     * @return all the players of a game.
     */
    public List<Player> getPlayers() {
    	return this.players;
    }
    
    /**
     * @return the board of a game.
     */
    public Board getBoard() {
    	return this.board;
    }

    /**
     * Starts the Tic Tac Toe game. <br>
     * Asks after each ended game if the user want to continue. Continues until
     * the user does not want to play anymore.
     * @throws InvalidMoveException when an invalid move is tried.
     */
    public void start() throws InvalidMoveException {
        boolean doorgaan = true;
        while (doorgaan) {
            this.reset();
            this.play();
            doorgaan = readBoolean("\n> Play another time? (y/n)?", "y", "n");
        }
    }

    /**
     * Prints a question which can be answered by yes (true) or no (false).
     * After prompting the question on standard out, this method reads a String
     * from standard in and compares it to the parameters for yes and no. If the
     * user inputs a different value, the prompt is repeated and te method reads
     * input again.
     * 
     * @parom prompt the question to print
     * @param yes
     *            the String corresponding to a yes answer
     * @param no
     *            the String corresponding to a no answer
     * @return true is the yes answer is typed, false if the no answer is typed
     */
    private boolean readBoolean(String prompt, String yes, String no) {
        String answer;
        do {
            System.out.print(prompt);
            try (Scanner in = new Scanner(System.in)) {
                answer = in.hasNextLine() ? in.nextLine() : null;
            }
        } while (answer == null || (!answer.equals(yes) && !answer.equals(no)));
        return answer.equals(yes);
    }

    /**
     * Resets the game. <br>
     * The board is emptied and player[0] becomes the current player.
     */
    private void reset() {
        this.current = 0;
        this.board.reset();
    }

    /**
     * Plays the Tic Tac Toe game. <br>
     * First the (still empty) board is shown. Then the game is played until it
     * is over. Players can make a move one after the other. After each move,
     * the changed game situation is printed.
     * @throws InvalidMoveException when a invalid move is tried.
     */
    private void play() throws InvalidMoveException {
        while (!this.gameOver()) {
        	this.players.get(current).makeMove(this.board);
        	this.update();
        	this.current = (this.current + 1) % numberPlayers;
        }
        ClientTUIView.print(this.getResult());
    }

    /**
     * Prints the game situation.
     */
    private void update() {
        System.out.println("\ncurrent game situation: \n\n" + board.toString()
                + "\n");
    }

    /*@
       requires this.board.gameOver();
     */

    /**
     * Prints the result of the last game. <br>
     */
    public String getResult() {
    	String result = "";
        if (this.hasWinner()) {
            Player winner = this.getWinnner();
            result = "Winner" + " " + winner.getName();
        } else {
            result = "Draw";
        }
        return result;
    }
}
