package controller;

import java.util.Scanner;

import exceptions.InvalidMoveException;
import model.Board;

/**
 * Class for maintaining a human player in Tic Tac Toe. Module 2 lab assignment
 * 
 * @author Theo Ruys
 * @version $Revision: 1.4 $
 */
public class NormalPlayer extends Player {

    // -- Constructors -----------------------------------------------

    /*@
       requires name != null;
       requires mark == Mark.XX || mark == Mark.OO;
       ensures this.getName() == name;
       ensures this.getMark() == mark;
    */
    /**
     * Creates a new human player object.
     * 
     */
    public NormalPlayer(String name) {
        super(name);
    }

    // -- Commands ---------------------------------------------------

    /*@
       requires board != null;
       ensures board.isField(\result) && board.isEmptyField(\result);

     */
    /**
     * Asks the user to input the field where to place the next mark. This is
     * done using the standard input/output. \
     * 
     * @param board
     *            the game board
     * @return the player's chosen field
     * @throws InvalidMoveException if the determined move is invalid
     */
    public synchronized int[] determineMove(Board board) throws InvalidMoveException {
    	boolean isValid = false;
    	int[] result = new int[Board.DIM];
    	String prompt1 = "> " + getName() + " (" + getName().toString() + ")"
    		+ ", chose your X? ";
    	int x = readInt(prompt1);

    	String prompt2 = "> " + getName() + " (" + getName().toString() + ")"
    		+ ", chose your Y? ";
    	int y = readInt(prompt2);
    	int z = board.getZ(x, y);
    	isValid = board.isField(x, y, z) && board.isEmptyField(x, y, z) && 
    		board.isValidMove(x, y, z);
    	if (!isValid) {
    		throw new InvalidMoveException(x, y);
    	} else {
    		result[0] = x;
    		result[1] = y;
    		result[2] = z;
    	}
    	return result;
    }
        

    /**
     * Writes a prompt to standard out and tries to read an int value from
     * standard in. This is repeated until an int value is entered.
     * 
     * @param prompt
     *            the question to prompt the user
     * @return the first int value which is entered by the user
     */
    private int readInt(String prompt) {
    	synchronized (System.in) {
    		int value = 0;
            boolean intRead = false;
            Scanner line = new Scanner(System.in);
            do {
                System.out.print(prompt);
                try (Scanner scannerLine = new Scanner(line.next());) {
                    if (scannerLine.hasNextInt()) {
                        intRead = true;
                        value = scannerLine.nextInt();
                    }
                }
            } while (!intRead);
            
            return value;
    	}
    }


}
