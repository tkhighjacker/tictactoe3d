package controller;

import model.Board;

public interface Strategy {
	public String getName();
	public int[] determineMove(Board b, String m);
}


