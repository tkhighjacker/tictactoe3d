package controller;

import exceptions.InvalidMoveException;
import model.Board;

public class ComputerPlayer extends Player {
	
	Strategy strategy;
	
	/**
	 * Constructor of computerplayer.
	 * @param name: the name of the player
	 * @param strategy: the strategy of the player.
	 */
	public ComputerPlayer(String name, Strategy strategy) {
		super(name + strategy.getName());
		this.strategy = strategy;
	}
	
	/**
	 * Constructor of computerplayer.
	 * @param name: the name of the player.
	 */
	public ComputerPlayer(String name) {
		this(name, new NaiveStrategy());
	}

	/**
     * Determines the field for the next move.
     * 
     * @param board
     *            the current game board
     * @return the player's choice
     * @throws InvalidMoveException when an invalid move is made
     */
	@Override
	public int[] determineMove(Board board) throws InvalidMoveException {
		int[] result = new int[Board.DIM];
		result = this.strategy.determineMove(board, this.getAbbreviation());
		int x = result[0];
		int y = result[1];
		int z = result[2];
		boolean isValid = board.isField(x, y, z) && board.isEmptyField(x, y, z) && 
    		board.isValidMove(x, y, z);
		if (!isValid) {
			throw new InvalidMoveException(x, y);
		}
		return result;
	}
	
	/**
	 * @return the strategy of the computerplayer.
	 */
	public Strategy getStrategy() {
		return this.strategy;
	}
	
	/**
	 * Sets the strategy of a player.
	 * @param strategy: the strategy 
	 */
	public void setStrategy(Strategy strategy) {
		this.strategy = strategy;
	}

}
