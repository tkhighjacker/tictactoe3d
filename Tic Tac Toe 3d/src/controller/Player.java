package controller;

import exceptions.InvalidMoveException;
import model.Board;

/**
 * Abstract class for keeping a player in the Tic Tac Toe game. Module 2 lab
 * assignment.
 * 
 * 
 * @author Theo Ruys en Arend Rensink
 * @version $Revision: 1.4 $
 */
public abstract class Player {

    // -- Instance variables -----------------------------------------

    private String name;
    private String abbreviation;

    // -- Constructors -----------------------------------------------

    /*@
       requires name != null;
       requires mark == Mark.XX || mark== Mark.OO;
       ensures this.getName() == name;
       ensures this.getMark() == mark;
     */
    /**
     * Creates a new Player object.
     * @throws InvalidNameException when an invallid name is entered.
     * 
     */
    public Player(String name) {
        this.name = name;
        this.abbreviation = name.substring(0, 3);
    }

    // -- Queries ----------------------------------------------------

    /**
     * Returns the name of the player.
     */
    /*@ pure */ public String getAbbreviation() {
        return this.abbreviation;
    }

    /**
     * Returns the mark of the player.
     */
    /*@ pure */ public String getName() {
        return this.name;
    }

    /*@
       requires board != null & !board.isFull();
       ensures board.isField(\result) & board.isEmptyField(\result);

     */
    /**
     * Determines the field for the next move.
     * 
     * @param board
     *            the current game board
     * @return the player's choice
     * @throws InvalidMoveException when an invalid move is made
     */
    public abstract int[] determineMove(Board board) throws InvalidMoveException;

    // -- Commands ---------------------------------------------------

    /*@
       requires board != null & !board.isFull();
     */
    /**
     * Makes a move on the board. <br>
     * 
     * @param board
     *            the current board
     * @throws InvalidMoveException when an invalid move is made
     */
    public void makeMove(Board board) throws InvalidMoveException {
    	int[] choice = determineMove(board);
        board.setField(choice[0], choice[1], choice[2], this.abbreviation);
    }

}
