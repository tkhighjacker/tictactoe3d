package controller;

import java.util.ArrayList;
import java.util.List;

import model.Board;

public class SmartStrategy implements Strategy {
	
	private String name;
	private int highestNewPossibilities;
	private int highestRowAdds;
	private int highestBlocks;
	private int[] smartResult;
	private List<String> players;

	/**
	 * Creates a new smart strategy with the name Smart.
	 */
	public SmartStrategy() {
		this.name = "Smart";
	}

	/**
	 * Returns the name of this strategy.
	 */
	@Override
	public String getName() {
		return this.name;
	}

	/**
	 * This method return the smart move on this board.
	 */
	@Override
	public int[] determineMove(Board board, String myPlayer) {
		this.createSmartMove(board, myPlayer);
		return this.smartResult;
	}
	
	/**
	 * This method creates a smart move for the smart strategy on 
	 * the given board for the given player and sets it.
	 * 
	 * @param board on which the smart move needs to be determined
	 * @param myPlayer for which the smart move needs to be determined
	 */
	public void createSmartMove(Board board, String myPlayer) {
		List<int[]> choices = getEmptyFields(board);
		smartResult = new int[3];
		this.players = board.getFieldNames();
		this.highestNewPossibilities = 0;
		this.highestRowAdds = 0;
		this.highestBlocks = 0;
		boolean hasWinningMove = false;
		for (int i = 0; i < choices.size() && !hasWinningMove; i++) {
			int[] array = choices.get(i);
			int x = array[0];
			int y = array[1];
			int z = array[2];
			Board copyWinBoard = board.deepCopy();			
			copyWinBoard.setField(x, y, z, myPlayer);
			int newPossibilities = board.countRowsContainMarkAtXYZ(x, y, z, "");
			if (copyWinBoard.isWinner(myPlayer)) {
				hasWinningMove = true;
				this.smartResult = array;
			} else if (hasOtherWinner(board, array)) {
				hasWinningMove = true;
				this.smartResult = array;
			} else if (this.players.isEmpty()) {
				this.checkNewPossibilities(newPossibilities, array);
			} else if (!this.players.contains(myPlayer)) {
				this.checkNewPossibilitiesBlocks(newPossibilities, array, board);
			} else if (this.players.contains(myPlayer)) {
				this.checkNewPossibilitiesRowAddsBlocks(newPossibilities, array, myPlayer, board);
			}
		}
	}
	
	/**
	 * Checks if the given board has an winner in the move in the array is played.
	 * 
	 * @param board on which the winning move needs to  be checked
	 * @param array which contains the move that needs to be checked.
	 * @return true if this board has an other winner than my player; false if not
	 */
	public boolean hasOtherWinner(Board board, int[] array) {
		boolean result = false;
		for (int i = 0; i < players.size() && !result; i++) {
			String player = players.get(i);
			Board copyBlockBoard = board.deepCopy();
			copyBlockBoard.setField(array[0], array[1], array[2], player);
			if (copyBlockBoard.isWinner(player)) {
				result = true;
				this.smartResult = array;
			}
		}
		return result;
	}
	
	/**
	 * this method sets the smart result if the amount of new possibilities 
	 * of this move exceeds that of an other move. 
	 * Only being used if there are no other players who made a move.
	 * 
	 * @param newPossibilities the amount of new possibilities this move creates
	 * @param array in which these new possibilities occur.
	 */
	public void checkNewPossibilities(int newPossibilities, int[] array) {
		if (newPossibilities > this.highestNewPossibilities) {
			this.smartResult = array;
			this.highestNewPossibilities = newPossibilities;
		}			
	}
	
	/**
	 * Checks the amount of blocks and new possibilities of this move and 
	 * checks if it exceeds the current best move, which is the one 
	 * with the highest blocks of the opponent. If so, it sets the smart field.
	 * Only used if myPlayer hasn't made a move yet.
	 * 
	 * @param newPossibilities of the current move.
	 * @param array of the field that needs to be checked
	 * @param b Board that this checks needs to be performed on
	 */
	public void checkNewPossibilitiesBlocks(int newPossibilities, int[] array, Board b) {
		for (String player : players) {
			int blocks = b.countRowsContainMarkAtXYZ(array[0], array[1], array[2], player);
			if (blocks > this.highestBlocks) {
				this.smartResult = array;
				this.highestNewPossibilities = newPossibilities;
				this.highestBlocks = blocks;
			} else if (blocks == this.highestBlocks) {
				if (newPossibilities > this.highestNewPossibilities) {
					this.smartResult = array;
					this.highestNewPossibilities = newPossibilities;
					this.highestBlocks = blocks;
				}
			}			
		}
	}	
	
	/**
	 * checks if this newPossibilities exceeds the current highest new possibilities.
	 * Also does it check if this move blocks the most rows of an other player.
	 * Finally does it check if this method makes all the most rows more complete
	 * 
	 * @param newPossibilities the amount of newPossibilities of this move
	 * @param array the array of this move
	 * @param myPlayer the Player that needs to be checked
	 * @param b Board on which this move needs to be checked
	 */
	public void checkNewPossibilitiesRowAddsBlocks(int newPossibilities, 
			int[] array, String myPlayer, Board b) {
		for (String player : players) {
			int rowAdds = b.countRowsContainMarkAtXYZ(array[0], array[1], array[2], myPlayer);
			int blocks = b.countRowsContainMarkAtXYZ(array[0], array[1], array[2], player);
			if (blocks > this.highestBlocks) {
				this.smartResult = array;
				this.highestNewPossibilities = newPossibilities;
				this.highestRowAdds = rowAdds;
				this.highestBlocks = blocks;
			} else if (rowAdds == this.highestRowAdds) {
				if (newPossibilities > this.highestNewPossibilities) {
					this.smartResult = array;
					this.highestNewPossibilities = newPossibilities;
					this.highestRowAdds = rowAdds;
					this.highestBlocks = blocks;
				} else if (blocks == this.highestBlocks) {
					if (rowAdds > this.highestRowAdds) {
						this.smartResult = array;
						this.highestNewPossibilities = newPossibilities;
						this.highestRowAdds = rowAdds;
						this.highestBlocks = blocks;
					}						
				}
			}			
		}
	}	
	
	/**
	 * Gets the empty Fields on this board.
	 * 
	 * @param b the Board that needs to be checked
	 * @return List with empty field arrays
	 */
	public static List<int[]> getEmptyFields(Board b) {
		List<int[]> result = new ArrayList<int[]>();
		for (int i = 0; i < Board.SIZE; i++) {
			for (int j = 0; j < Board.SIZE; j++) {
				for (int k = 0; k < Board.SIZE; k++) {
					if (b.isEmptyField(i, j, k) && b.isValidMove(i, j, k)) {
						result.add(new int[]{i, j, k});
					}
				}
			}
		}
		return result;
	}
}
