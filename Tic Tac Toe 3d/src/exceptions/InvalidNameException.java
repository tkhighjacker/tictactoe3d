package exceptions;

public class InvalidNameException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -424578944130319991L;

	public InvalidNameException(String s) {
		super(s + "is an invallid name");
	}
}
