package exceptions;

import controller.Player;
import protocol.Protocol;

public class InvalidMoveException extends Exception implements Protocol {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2295213860507123791L;

	public InvalidMoveException(int x, int y) {
		super("Move X: " + x + " Y: " + y + " is an invallid move");
	}
	
	public InvalidMoveException(int x, int y, Player player) {
		super(player.getName() + DILEM + x + " Y: " + y + " is an invallid move");
	}

}
