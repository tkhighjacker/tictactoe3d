package model;

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import org.apache.commons.lang3.StringUtils;

/**
 * Game student for the Tic Tac Toe game. Module 2 lab assignment.
 *
 * @author Theo Ruys en Arend Rensink
 * @version $Revision: 1.4 $
 */
public class Board extends Observable {
    public static final int SIZE = 4;
    public static final int DIM = 3;
    public static final int POINTS = (int) Math.pow(SIZE, DIM);
    public static final int LINES = ((int) Math.pow(SIZE + 2, DIM) - POINTS) / 2;
    private static final String LINE = "----+";

    /**
     * The SIZE^DIM fields of the Tic Tac Toe student. See NUMBERING for the
     * coding of the fields.
     */
    //@ private invariant fields.length == POINTS;
    /*@ invariant (\forall int i, j, k; 0 <= i & i < SIZE & 0 <= j & j < 
        SIZE & k <= 0 & k < SIZE; getField(i, j, k) != null; 
     */
    private String[][][] fields;
    private List<String> fieldNames;

    // -- Constructors -----------------------------------------------

    public List<String> getFieldNames() {
		return fieldNames;
	}

	/**
     * Creates an empty student.
     */
    /*@ ensures (\forall int i, j, k; 0 <= i & i < SIZE & 0 <= j & j < 
        SIZE & k <= 0 & k < SIZE; this.getField(i, j ,k).equals(""); 
        ensures fieldNames.contains("");
     */
    public Board() {    	
    	this.fields = new String[SIZE][SIZE][SIZE];
    	fieldNames = new ArrayList<>();
    	for (int i = 0; i < SIZE; i++) {
    		for (int j = 0; j < SIZE; j++) {
    			for (int k = 0; k < SIZE; k++) {
    				fields[i][j][k] = "";
    			}
    		}
    		
    	}
    }
    
    // -- Commands -----------------------------------------------
    
    /**
     * Creates a deep copy of this board.
     */
    /*@ ensures \result != this;
        ensures (\forall int i, j, k; 0 <= i & i < SIZE & 0 <= j & j < 
        SIZE & k <= 0 & k < SIZE; \result.getField(i, j ,k).equals(this.getField(i, j, k));
      @*/
    public Board deepCopy() {
    	Board board = new Board();
    	for (int i = 0; i < Board.SIZE; i++) {
    		for (int j = 0; j < Board.SIZE; j++) {
    			for (int k = 0; k < Board.SIZE; k++) {
    				String field = this.getField(i, j, k);
    				board.fields[i][j][k] = field;
    			}
    		}
    	}
    	return board;
    }
    
//    /**
//     * Turns the board 90 degrees clockwise.
//     * @return
//     */
//    public Board rotate90deg() {
//    	Board board = new Board();
//    	for (int z = 0; z < SIZE; z++) {
//    		for (int y = 0; y < SIZE; y++) {
//    			for (int x = 0; x < SIZE; x++) {
//    				Mark m = this.getField(x, y, z);
//    				board.setField(SIZE - y - 1, x, z, m);
//    			}
//    		}
//    	}
//    	return board;
//    }
    
    /**
     * Calculates the index in the linear array of fields from a (x, y, z)
     * pair.
     * @return the index belonging to the (x, y, z)-field
     */
    //@ requires 0 <= x & x < SIZE;
    //@ requires 0 <= y & y < SIZE;
    //@ requires 0 <= z & z < SIZE;
    //@ ensures \result < POINTS && \result >= 0;
    /*@pure*/
    public static int index(int x, int y, int z) {
    	return x + (y * SIZE) + (z * SIZE * SIZE);
    }
    
    
    public int getZ(int x, int y) {
    	int result = -1;
    	boolean found = false;
    	for (int i = 0; i < SIZE && !found; i++) {
    		if (this.isEmptyField(x, y, i)) {
    			result = i;
    			found = true;
    		}
    	}
    	return result;
    }

    /**
     * Returns true if the (x, y, z) pair refers to a valid field on the student.
     *
     * @return true if 0 <= x < SIZE && 0 <= y < SIZE && 0 <= z < SIZE 
     */
    //@ ensures \result == (0 <= x && x < SIZE && 0 <= y && y < SIZE && 0 <= z && y < SIZE);
    /*@pure*/
    public boolean isField(int x, int y, int z) {
    	return 0 <= x && x < SIZE && 0 <= y && y < SIZE && 0 <= z && z < SIZE;
    }
    
    /**
     * returns true if the field refers to a field at z = 0 or has an Mark underneath this position.
     * 
     * @param x location on the field
     * @param y location on the field
     * @param z location on the field
     * @return true if the index underneath this position is empty; false otherwise.
     */
    //@ ensures \result == (z == 0) || isEmptyField(x, y, z - 1);
    /*@pure*/
    public boolean isValidMove(int x, int y, int z) {
    	boolean result = false;
    	if (z == 0) {
    		result = true;
    	} else {
    		result = !this.isEmptyField(x, y, z - 1) && this.isEmptyField(x, y, z);
    	}
    	return result;
    }

    /**
     * Returns the content of the field referred to by the (x, y, z) pair.
     *
     * @param x
     *            the row of the field
     * @param y
     *            the column of the field
     * @param z
     * 			  the depth of the field           
     * @return the mark on the field
     */
    //@ requires this.isField(x, y, z);
    //@ ensures \result != null;
    /*@pure*/
    public String getField(int x, int y, int z) {
    	String result = "";
    	if (this.isField(x, y, z)) {
    		result = this.fields[x][y][z];
    	}
    	return result;
    }


    /**
     * Returns true if the field referred to by the (x ,y, z) pair it empty.
     *
     * @param x
     *            the row of the field
     * @param y
     *            the column of the field
     * @param z
     * 			  the depth of the field
     * @return true if the field is empty; false otherwise.
     */
    //@ ensures \result == (this.isField(x, y, z) & this.getField(x, y, z).equals("");
    /*@pure*/
    public boolean isEmptyField(int x, int y, int z) {
    	boolean result = false;    	
    	if (this.isField(x, y, z) && fields[x][y][z].equals("")) {
    		result = true;
    	}
    	return result;
    }
    
    /**
     * Tests if the whole student is full.
     *
     * @return true if all fields are occupied
     */
    /*@ ensures \result == (\forall int i, j, k; 0 <= i & i < SIZE & 0 <= j & j < 
        SIZE & k <= 0 & k < SIZE; this.getField(i, j, k) != "");
     */
    /*@pure*/
    public boolean isFull() {
    	boolean result = true;
    	for (int i = 0; i < SIZE && result; i++) {
    		for (int j = 0; j < SIZE && result; j++) {
    			for (int k = 0; k < SIZE && result; k++) {
    				if (this.isEmptyField(i, j, k)) {
    	    			result = false;
    				}
    			}
    		}
    	}
    	return result;
    }
    
    public boolean isEmptyBoard() {
    	boolean result = true;
    	for (int i = 0; i < SIZE && result; i++) {
    		for (int j = 0; j < SIZE && result; j++) {
    			for (int k = 0; k < SIZE && result; k++) {
    				if (!this.isEmptyField(i, j, k)) {
    					result = false;
    				}
    			}
    		}
    	}
    	return result;
    }

    /**
     * Checks whether a player has an row in the X, Y, Z axis.
     * 
     * @param s
     * 			the mark of interest
     * @return true if there is a row controlled by m; false if not
     */
    //@ ensures \result == (this.hasRowX(m) || this.hasRowY(m) || this.hasRowZ(m));
    /*@ pure */
    public boolean hasRow(String s) {
    	return this.hasRowX(s) || this.hasRowY(s) || this.hasRowZ(s);
    }

    /**
     * Checks whether there is a row in the XY pane which is full and only contains the mark
     * m.
     *
     * @param s
     *            the mark of interest
     * @return true if there is a row in XY pane controlled by m; false if not
     */
    /*@ pure */
    public boolean hasRowX(String s) {
    	boolean result = false;
    	for (int z = 0; z < SIZE && !result; z++) {
    		for (int y = 0; y < SIZE && !result; y++) {
    			int counter = 0;
        		for (int x = 0; x < SIZE; x++) {
        			if (this.getField(x, y, z).equals(s)) {
        				counter++;
        				result = counter == SIZE;
        			}
        		}
    		}
    	}
        return result;
    }
    
    /**
     * Checks whether there is a row in the XZ pane which is full and only contains the mark
     * m.
     *
     * @param s
     *            the mark of interest
     * @return true if there is a row in XZ pane controlled by m; false if not
     */
    /*@ pure */
    public boolean hasRowY(String s) {
    	boolean result = false;
    	for (int x = 0; x < SIZE && !result; x++) {
    		for (int z = 0; z < SIZE && !result; z++) {
        		int counter = 0;
        		for (int y = 0; y < SIZE; y++) {
        			if (this.getField(x, y, z).equals(s)) {
        				counter++;
        				result = counter == SIZE;
        			}
        		}
    		}
    	}
        return result;
    }
    
    /**
     * Checks whether there is a row in the YZ pane which is full and only contains the mark
     * m.
     *
     * @param s
     *            the mark of interest
     * @return true if there is a row in YZ pane controlled by m; false if not
     */
    /*@ pure */
    public boolean hasRowZ(String s) {
    	boolean result = false;
    	for (int x = 0; x < SIZE && !result; x++) {
    		for (int y = 0; y < SIZE && !result; y++) {
        		int counter = 0;
        		for (int z = 0; z < SIZE; z++) {
        			if (this.getField(x, y, z).equals(s)) {
        				counter++;
        				result = counter == SIZE;
        			}
        		}
    		}
    	}
        return result;
    }
    
    /**
     * Checks whether there is a diagonal which is full and only contains the
     * mark m.
     *
     * @param s
     *            the mark of interest
     * @return true if there is a diagonal controlled by m
     */
    /*@ pure */
    public boolean hasDiagonal(String s) {

    	return this.hasDiagonalXY(s) || this.hasDiagonalXZ(s) || 
    			this.hasDiagonalYZ(s) || this.hasDiagonalXYZ(s);
    }

    /**
     * Checks whether there is a diagonal on the XY axis which is full and only contains the
     * mark m.
     *
     * @param s
     *            the mark of interest
     * @return true if there is a diagonal on the XY axis controlled by m
     */
    /*@ pure */
    public boolean hasDiagonalXY(String s) {
    	boolean result = false;
    	for (int i = 0; i < SIZE && !result; i++) {
    		int counterUpDownXY = 0;
        	int counterDownUpXY = 0;
    		for (int j = 0; j < SIZE && !result; j++) {   		
        		if (this.getField(j, j, i).equals(s)) {
        			counterUpDownXY++;
        			
        		}
        		if (this.getField(j, SIZE - 1 - j, i).equals(s)) {
        			counterDownUpXY++;
        			
        		}
        	}
        	if (counterUpDownXY == SIZE || counterDownUpXY == SIZE) {
    			result = true;
        	}
    	}
        return result;
    }
    
    /**
     * Checks whether there is a diagonal on the XZ axis which is full and only contains the
     * mark m.
     *
     * @param s
     *            the mark of interest
     * @return true if there is a diagonal on the XZ axis controlled by m
     */
    /*@ pure */
    public boolean hasDiagonalXZ(String s) {
    	boolean result = false;
    	for (int i = 0; i < SIZE && !result; i++) {
    		int counterUpDownXZ = 0;
        	int counterDownUpXZ = 0;
    		for (int j = 0; j < SIZE && !result; j++) {   		
        		if (this.getField(j, i, j).equals(s)) {
        			counterUpDownXZ++;
        			
        		}
        		if (this.getField(j, i, SIZE - 1 - j).equals(s)) {
        			counterDownUpXZ++;
        			
        		}
        	}
        	if (counterUpDownXZ == SIZE || counterDownUpXZ == SIZE) {
    			result = true;
        	}
    	}
        return result;
    }
    
    /**
     * Checks whether there is a diagonal on the YZ axis which is full and only contains the
     * mark m.
     *
     * @param s
     *            the mark of interest
     * @return true if there is a diagonal on the YZ axis controlled by m
     */
    /*@ pure */
    public boolean hasDiagonalYZ(String s) {
    	boolean result = false;
    	for (int i = 0; i < SIZE && !result; i++) {
    		int counterUpDownYZ = 0;
        	int counterDownUpYZ = 0;
    		for (int j = 0; j < SIZE && !result; j++) {
    			
        		if (this.getField(i, j, j).equals(s)) {
        			counterUpDownYZ++;
        			
        		}
        		if (this.getField(i, SIZE - 1 - j, j).equals(s)) {
        			counterDownUpYZ++;
        			
        		}
        	}
        	if (counterUpDownYZ == SIZE || counterDownUpYZ == SIZE) {
    			result = true;
        	}
    	}
        return result;
    }
    
    /**
     * Checks whether there is a diagonal on the XYZ axis which is full and only contains the
     * mark m.
     *
     * @param s
     *            the mark of interest
     * @return true if there is a diagonal on the XYZ axis controlled by m
     */
    /*@ pure */
    public boolean hasDiagonalXYZ(String s) {
    	boolean result = false;
    	int counterUpDownRightXYZ = 0;
    	int counterDownUpRightXYZ = 0;
    	int counterUpDownLeftXYZ = 0;
    	int counterDownUpLeftXYZ = 0;
    	for (int i = 0; i < SIZE && !result; i++) {
    		if (this.getField(i, i, i).equals(s)) {
        		counterUpDownRightXYZ++;
        		
        	}
        	if (this.getField(SIZE - i - 1, i, i).equals(s)) {
        		counterUpDownLeftXYZ++;
        		
        	}
        	if (this.getField(i, SIZE - i - 1, i).equals(s)) {
        		counterDownUpRightXYZ++;
        		
        	}
        	if (this.getField(i, i, SIZE - i - 1).equals(s)) {
        		counterDownUpLeftXYZ++;
        		
        	}
        	
        	
        	if (counterUpDownRightXYZ == SIZE || counterDownUpRightXYZ == SIZE 
        			|| counterUpDownLeftXYZ == SIZE || counterDownUpLeftXYZ == SIZE) {
        		result = true;
        	}
    	}
    	return result;
    }

    /**
     * Checks if the mark m has won. A mark wins if it controls at
     * least one row or diagonal.
     *
     * @param s
     *            the mark of interest
     * @return true if the mark has won
     */
    //@requires m == Mark.XX | m == Mark.OO;
    //@ ensures \result == this.hasRow(m) || this.hasDiagonal(m);
    /*@ pure */
    public boolean isWinner(String s) {
    	return this.hasRow(s) || this.hasDiagonal(s);
    }

    /**
     * Empties all fields of this student (i.e., let them refer to the value
     * "").
     */
    /*@ ensures (\forall int i; 0 <= i & i < POINTS;
                                this.getField(i).equals(""); @*/
    public void reset() {
    	for (int i = 0; i < SIZE; i++) {
    		for (int j = 0; j < SIZE; j++) {
    			for (int k = 0; k < SIZE; k++) {
    				this.fields[i][j][k] = "";
    			}
    		}
    	}
    }

    /**
     * Sets the content of the field represented by the (x, y, z) pair to the
     * mark m.
     *
     * @param x
     *            the field's row
     * @param y
     *            the field's column
     * @param z
     * 			  the field's depth
     * @param s
     *            the mark to be placed
     */
    //@ requires this.isField(x, y, z);
    /*@ ensures this.getField(x, y, z).equals(s);
        ensures this.getFieldNames.contains(s);
     */
    public void setField(int x, int y, int z, String s) {
    	if (this.isField(x, y, z)) {
    		fields[x][y][z] = s;
    		this.setChanged();
    		this.notifyObservers();
    		if (!fieldNames.contains(s) || !s.equals("")) {
    			fieldNames.add(s);
    		}
    	}
    }
    
    /**
     * Checks if the current board equals the input board.
     * 
     * @param 
     * 			board to be compared
     * 
     * @return true if input board has the same fields as this board; false otherwise
     */
    //@ requires board != null;
    //@ ensures \forall int i; 0 < i && i < POINTS; this.getField(i) == board.getField(i);
    public boolean equals(Board board) {
		boolean result = true;
		for (int i = 0; i < SIZE && result; i++) {
			for (int j = 0; j < SIZE && result; j++) {
				for (int k = 0; k < SIZE && result; k++) {
					if (this.getField(i, j, k) != board.getField(i, j, k)) {
						result = false;
					}
				}
			}
			
		}
    	return result;
    	
    }
    
    /**
     * Returns a String representation of this student. In addition to the current
     * situation, the String also shows the numbering of the fields.
     *
     * @return the game situation as String
     */
    public String toString() {
        String s = "";
        for (int z = 0; z < SIZE; z++) {
        	if (z != 0) {
        		s = s + "\n\n";
        	} 
        	s = s + "Z = " + z + "\n";
        	s = s + "Y  X";
        	for (int i = 0; i < SIZE; i++) {
        		s = s +  StringUtils.center(i + "", 4);	
        		if (i < SIZE - 1) {
                    s = s + "|";
                }
        	}
        	s = s + "\n";
        	for (int y = 0; y < SIZE; y++) {
                String row = StringUtils.center(y + "", 4);
                for (int x = 0; x < SIZE; x++) {
                    row = row + StringUtils.center(this.getField(x, y, z), 4);
                    if (x < SIZE - 1) {
                        row = row + "|";
                    }
                }
                s = s + row;
                s = s + "\n" + "    ";
                if (y < SIZE - 1) {    	
                	for (int i = 0; i < SIZE - 1; i++) {
                		s = s + LINE;
                	}
                	s = s + "----\n";
                    
                
                }
        	}
        }
        return s;
    }
    
/*---------------------------Smart strategy aid-----------------------------*/
    
    public int countRowsContainMarkAtXYZ(int x, int y, int z, String s) {
    	int result = 0;
    	if (this.rowXContainsMark(y, z, s)) {
    		result++;
    	}
    	if (this.rowYContainsMark(x, z, s)) {
    		result++;
    	}
    	if (this.rowZContainsMark(x, y, s)) {
    		result++;
    	}
    	if (this.diagonalRowDownXYContainsMark(x, y, z, s)) {
    		result++;
    	}
    	if (this.diagonalRowUpXYContainsMark(x, y, z, s)) {
    		result++;
    	}
    	if (this.diagonalRowDownXZContainsMark(x, y, z, s)) {
    		result++;
    	}
    	if (this.diagonalRowUpXZContainsMark(x, y, z, s)) {
    		result++;
    	}
    	if (this.diagonalRowDownYZContainsMark(x, y, z, s)) {
    		result++;
    	}
    	if (this.diagonalRowUpYZContainsMark(x, y, z, s)) {
    		result++;
    	}
    	if (this.diagonalRowDownLeftXYZContainsMark(x, y, z, s)) {
    		result++;
    	}
    	if (this.diagonalRowDownRightXYZContainsMark(x, y, z, s)) {
    		result++;
    	}
    	if (this.diagonalRowUpLeftXYZContainsMark(x, y, z, s)) {
    		result++;
    	}
    	if (this.diagonalRowUpRightXYZContainsMark(x, y, z, s)) {
    		result++;
    	}
    	return result;
    }
    
    /**
     * Checks if there is a mark in the x rows.
     * @param y: the y of the field which is being checked.
     * @param z:the z of the field which is being checked.
     * @param s: the playerabrreviation.
     * @return true if there is a mark in row x.
     */
    public boolean rowXContainsMark(int y, int z, String s) {
    	String[] sRow = this.getRowX(y, z);
    	return this.checkRow(sRow, s);
    }
    
    /**
     * @param y: the y of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @return a row x with given y and z.
     */
    public String[] getRowX(int y, int z) {
    	String[] result = new String[Board.SIZE];
    	for (int x = 0; x < Board.SIZE; x++) {
    		result[x] = this.getField(x, y, z);
    	}
    	return result;
    }
    
    /**
     * Checks if there is a mark in the y rows.
     * @param x: the x of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @param s: the playerabrreviation.
     * @return true if there is a mark in row y.
     */
    public boolean rowYContainsMark(int x, int z, String s) {
    	String[] sRow = this.getRowX(x, z);
    	return this.checkRow(sRow, s);
    	
    }
    
    /**
     * @param x: the x of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @return a row y with given x and z.
     */
    public String[] getRowY(int x, int z) {
    	String[] result = new String[Board.SIZE];
    	for (int y = 0; y < Board.SIZE; y++) {
    		result[y] = this.getField(y, x, z);
    	}
    	return result;
    }
    
  /**
    * Checks if there is a mark in the z rows.
    * @param x: the x of the field which is being checked.
    * @param y: the y of the field which is being checked.
    * @param s: the playerabrreviation.
    * @return true if there is a mark in row z.
    */
    public boolean rowZContainsMark(int x, int y, String s) {
    	String[] sRow = this.getRowX(x, y);
    	return this.checkRow(sRow, s);
    	
    }
    
    /**
     * @param x: the x of the field which is being checked.
     * @param y: the y of the field which is being checked.
     * @return a row z with given x and y.
     */
    public String[] getRowZ(int x, int y) {
    	String[] result = new String[Board.SIZE];
    	for (int z = 0; z < Board.SIZE; z++) {
    		result[z] = this.getField(x, y, z);
    	}
    	return result;
    }
    
    /**
     * Checks if a diagonal row in xy up contains a mark.
     * @param x: the x of the field which is being checked.
     * @param y: the y of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @param s: the player abbreviation.
     * @return true if there is a mark in this diagonal row.
     */
    public boolean diagonalRowUpXYContainsMark(int x, int y, int z, String s) {
    	boolean result = false;
    	if (x == y) {
    		String[] sRow = this.getDiagonalRowUpXY(z);
    		result = this.checkRow(sRow, s);
    	}
    	return result;
    	
    }
        
    /**
     * @param z: the z of the field which is being checked.
     * @return a diagonal row with given z.
     */
    public String[] getDiagonalRowUpXY(int z) {
    	String[] result = new String[Board.SIZE];	
    	for (int i = 0; i < Board.SIZE; i++) {
    		result[i] = this.getField(i, i, z); 		        	
    	}
    	return result;
    }
    
    /**
     * Checks if a diagonal row in xy down contains a mark.
     * @param x: the x of the field which is being checked.
     * @param y: the y of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @param s: the player abbreviation.
     * @return true if there is a mark in this diagonal row.
     */
    public boolean diagonalRowDownXYContainsMark(int x, int y, int z, String s) {
    	boolean result = false;
    	if (x == y) {
    		String[] sRow = this.getDiagonalRowUpXY(z);
        	result = this.checkRow(sRow, s);
    	}
    	return result;
    }
    
    /**
     * @param z: the z of the field which is being checked.
     * @return a diagonal row with given z.
     */
    public String[] getDiagonalRowDownXY(int z) {
    	String[] result = new String[Board.SIZE];
    	for (int i = 0; i < Board.SIZE; i++) {
        	result[i] = this.getField(i, SIZE - 1 - i, z);   		
    	}
    	return result;
    }
    
    /**
     * Checks if a diagonal row in xz up contains a mark.
     * @param x: the x of the field which is being checked.
     * @param y: the y of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @param s: the player abbreviation.
     * @return true if there is a mark in this diagonal row.
     */
    public boolean diagonalRowUpXZContainsMark(int x, int y, int z, String s) {
    	boolean result = false;
    	if (x == z) {
    		String[] sRow = this.getDiagonalRowUpXZ(y);
        	result = this.checkRow(sRow, s);
    	}
    	return result;
    }
    
    /**
     * @param y: the y of the field which is being checked.
     * @return a diagonal row with given y.
     */
    public String[] getDiagonalRowUpXZ(int y) {
    	String[] result = new String[Board.SIZE];
    	for (int i = 0; i < Board.SIZE; i++) {
    		result[i] = this.getField(i, y, i);   		
    	}
    	return result;
    }
    
    /**
     * Checks if a diagonal row in xz down contains a mark.
     * @param x: the x of the field which is being checked.
     * @param y: the y of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @param s: the player abbreviation.
     * @return true if there is a mark in this diagonal row.
     */
    public boolean diagonalRowDownXZContainsMark(int x, int y, int z, String s) {
    	boolean result = false;
    	if (x == z) {
    		String[] sRow = this.getDiagonalRowDownXZ(y);
        	result = this.checkRow(sRow, s);
    	}
    	return result;
    }
    
    /**
     * @param y: the y of the field which is being checked.
     * @return a diagonal row with given y.
     */
    public String[] getDiagonalRowDownXZ(int y) {
    	String[] result = new String[Board.SIZE];
    	for (int i = 0; i < Board.SIZE; i++) {
    		result[i] = this.getField(SIZE - 1 - i, y, SIZE - 1 - i);   		
    	}
    	return result;
    }
    
    /**
     * Checks if a diagonal row in yz up contains a mark.
     * @param x: the x of the field which is being checked.
     * @param y: the y of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @param s: the player abbreviation.
     * @return true if there is a mark in this diagonal row.
     */
    public boolean diagonalRowUpYZContainsMark(int x, int y, int z, String s) {
    	boolean result = false;
    	if (y == z) {
    		String[] sRow = this.getDiagonalRowUpYZ(x);
    		result = this.checkRow(sRow, s);
    	}
    	return result;
    }
    
    /**
     * @param x: the x of the field which is being checked.
     * @return a diagonal row with given x.
     */
    public String[] getDiagonalRowUpYZ(int x) {
    	String[] result = new String[Board.SIZE];
    	for (int i = 0; i < Board.SIZE; i++) {
    		result[i] = this.getField(x, i, i);   		        	
    	}
    	return result;
    }
    
    /**
     * Checks if a diagonal row in yz down contains a mark.
     * @param x: the x of the field which is being checked.
     * @param y: the y of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @param s: the player abbreviation.
     * @return true if there is a mark in this diagonal row.
     */
    public boolean diagonalRowDownYZContainsMark(int x, int y, int z, String s) {
    	boolean result = false;
    	if (y == z) {
    		String[] sRow = this.getDiagonalRowDownYZ(x);
    		result = this.checkRow(sRow, s);
    	}
    	return result;
    }
    
    /**
     * @param x: the x of the field which is being checked.
     * @return a diagonal row with given x.
     */
    public String[] getDiagonalRowDownYZ(int x) {
    	String[] result = new String[Board.SIZE];
    	for (int i = 0; i < Board.SIZE; i++) {
    		result[i] = this.getField(x, SIZE - 1 - i, SIZE - 1 - i);   		
    	}	
    	return result;
    }
    
    /**
     * Checks if a diagonal row in xyz up right contains a mark.
     * @param x: the x of the field which is being checked.
     * @param y: the y of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @param s: the player abbreviation.
     * @return true if there is a mark in this diagonal row.
     */
    public boolean diagonalRowUpRightXYZContainsMark(int x, int y, int z, String s) {
    	boolean result = false;
    	if (x == y && y == z) {
    		String[] sRow = this.getDiagonalRowUpRightXYZ();
    		result = this.checkRow(sRow, s);
    	}
    	return result;
    }
    
    /**
     * @return a diagonal row up right.
     */
    public String[] getDiagonalRowUpRightXYZ() {
    	String[] result = new String[Board.SIZE];
    	for (int i = 0; i < Board.SIZE; i++) {
    		result[i] = this.getField(i, i, i);   		
    	} 
    	return result;
    }
    
    /**
     * Checks if a diagonal row in xyz up left contains a mark.
     * @param x: the x of the field which is being checked.
     * @param y: the y of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @param s: the player abbreviation.
     * @return true if there is a mark in this diagonal row.
     */
    public boolean diagonalRowUpLeftXYZContainsMark(int x, int y, int z, String s) {
    	boolean result = false;
    	if (x == y && y == z) {
    		String[] sRow = this.getDiagonalRowUpLeftXYZ();
    		result = this.checkRow(sRow, s);
    	}
    	return result;
    }
    
    /**
     * @return a diagonal row left right.
     */
    public String[] getDiagonalRowUpLeftXYZ() {
    	String[] result = new String[Board.SIZE];
    	for (int i = 0; i < Board.SIZE; i++) {
    		result[i] = this.getField(SIZE - 1 - i, i, i);   		
    	} 
    	return result;
    }
    
    /**
     * Checks if a diagonal row in xyz down right contains a mark.
     * @param x: the x of the field which is being checked.
     * @param y: the y of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @param s: the player abbreviation.
     * @return true if there is a mark in this diagonal row.
     */
    public boolean diagonalRowDownRightXYZContainsMark(int x, int y, int z, String s) {
    	boolean result = false;
    	if (x == y && y == z) {
    		String[] sRow = this.getDiagonalRowDownRightXYZ();
    		result = this.checkRow(sRow, s);
    	}
    	return result;
    }
    
    /**
     * @return a diagonal row down right.
     */
    public String[] getDiagonalRowDownRightXYZ() {
    	String[] result = new String[Board.SIZE];
    	for (int i = 0; i < Board.SIZE; i++) {
    		result[i] = this.getField(i, SIZE - 1 - i, i);   		

    	} 
    	return result;
    }
    
    /**
     * Checks if a diagonal row in xyz down left contains a mark.
     * @param x: the x of the field which is being checked.
     * @param y: the y of the field which is being checked.
     * @param z: the z of the field which is being checked.
     * @param s: the player abbreviation.
     * @return true if there is a mark in this diagonal row.
     */
    public boolean diagonalRowDownLeftXYZContainsMark(int x, int y, int z, String s) {
    	boolean result = false;
    	if (x == y && y == z) {
    		String[] sRow = this.getDiagonalRowDownLeftXYZ();
    		result = this.checkRow(sRow, s);
    	}
    	return result;
    }
    
    /**
     * @return a diagonal row down left.
     */
    public String[] getDiagonalRowDownLeftXYZ() {
    	String[] result = new String[Board.SIZE];
    	for (int i = 0; i < Board.SIZE; i++) {
    		result[i] = this.getField(SIZE - 1 - i, SIZE - 1 - i, i);      	
    	} 
    	return result;
    }
    
    /**
     * Checks if a row contains a mark.
     * @param sRow: the row which you want you check.
     * @param s: the player abbreviation.
     * @return true if the row contains a mark.
     */
    public boolean checkRow(String[] sRow, String s) {
    	boolean result = false;
    	for (int i = 0; i < SIZE && !result; i++) {
    		if (sRow[i].equals(s)) {
    			result = true;
    		}
    	}
    	return result;
    }
}
