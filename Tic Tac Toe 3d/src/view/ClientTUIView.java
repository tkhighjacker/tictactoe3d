package view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Observable;
import java.util.Observer;

import org.apache.commons.validator.routines.InetAddressValidator;

import client.Client;
import controller.ComputerPlayer;
import controller.NormalPlayer;
import controller.Player;
import controller.SmartStrategy;
import model.Board;

public class ClientTUIView implements ClientView, Observer {
	
	private Client client;

	public ClientTUIView(Client client) {
		this.client = client;
	}

	@Override
	public void update(Observable object, Object arg1) {
		Board board = (Board) object;
		board.toString();
	}

	@Override
	public void showPlayers() {
	}

	@Override
	public void showBoard() {
		
	}

	@Override
	public void showCurrentPlayer() {
		
	}

	@Override
	public void showResult() {
		
	}

	@Override
	public void start() {

	}
	
	public void startUp() {
		this.client.setSocket(this.startUpSocket());
	}
	
	/**
	 * Asks for a port number and a corresponding IP address of the host. 
	 * When the IP address is invalid or the port already in use a error message will be printed.
	 * If everything went right a ServerSocket with given IP and port number will be created
	 */
	public InetAddress startUpHost() {
		//This will set the ip address to the designated server.
		String tempHost = "";
		InetAddress host = null;
		boolean rightHost = false;
		while (!rightHost) {
			tempHost = ClientTUIView.readString(
				"Enter the corresponding IP address of the host: ");	
			try {	
				if (InetAddressValidator.getInstance().isValid(tempHost)) {
					host = InetAddress.getByName(tempHost);
					rightHost = true;
				} else {
					ClientTUIView.print("Error: invallid ip adress");
				}
			} catch (UnknownHostException e) {
				ClientTUIView.print("Error: no valid hostname entered. Try again!");
			}
		}
		return host;
	}
	
	public int startUpPort() {
		//This will set the port of the designated server.
		String message = null;
		int port = 0;
		int tempPort = 0;
		boolean rightPort = false;
		while (!rightPort) {
			message = ClientTUIView.readString("Enter the port the host is listening to: ");
			try {
				tempPort = Integer.parseInt(message);
				if (tempPort >= 1 && tempPort <= 65535) {
					port = tempPort;
					rightPort = true;
				} else {
					ClientTUIView.print("Invalid port number, enter a port between 1 ~ 65535");
					
				}
			} catch (NumberFormatException e) {
				ClientTUIView.print("Error: no valid portnumber entered. Try again!");
			}
		}
		return port;
	}
		
	public Socket startUpSocket() {
		Socket sock = null;
		InetAddress address = this.startUpHost();
		int port = this.startUpPort();
		try {
			sock = new Socket(address, port);
		} catch (UnknownHostException e) {
			ClientTUIView.printError(e.getMessage());
			this.startUpHost();
		} catch (IOException e) {
			ClientTUIView.printError(e.getMessage());
			this.startUpPort();
		}
		return sock;
	}
	
	/**
	 * A method to choose if you want to play as a normal or computer player.
	 * When you enter AI or HUM you will be asked to enter your name.
	 * Afterwards a player will be created with the given name.
	 */
	public void chooseClient() {
		String message = "";
		Player myPlayer = null;
		message = ClientTUIView.readString("Choose your player: 'AI' for a "
				+ "computerplayer or 'HUM' for a humanplayer.");
		message = message.toLowerCase();
		if (message.equals("ai")) {
			message = "";
			while (message.equals("")) {
				message = ClientTUIView.readString("Please type your name.");
			}
			myPlayer = new ComputerPlayer(message, new SmartStrategy());
			this.client.setMyPlayer(myPlayer);
			ClientTUIView.print("You succesfully chose to play as AI with name: " 
				+ myPlayer.getName());
			
		} else if (message.equals("hum")) {
			message = "";
			while (message.equals("")) {
				message = ClientTUIView.readString("Please type your name.");
			}
			myPlayer = new NormalPlayer(message);
			this.client.setMyPlayer(myPlayer);
			ClientTUIView.print("You succesfully chose to play as HUM with name: "
				+ myPlayer.getName());
		} else {
			ClientTUIView.print("No correct choice, made a computerplayer with the given input.");
			myPlayer = new ComputerPlayer(message, new SmartStrategy());
			this.client.setMyPlayer(myPlayer);
		}
		this.client.setMyPlayer(myPlayer);
	}

	/**
	 * A method which will make printing messages easier.
	 * @param message: the message you want to print.
	 */
	public static void print(String message) {
		System.out.println(message);
	}

	/**
	 * A method which will make printing messages easier.
	 * @param message: the message you want to print.
	 */
	public static void printError(String message) {
		System.err.println(message);
	}

	/**
	 * A method which creates a BufferedReader which you can use.
	 * @param message: the message you want to read.
	 */
	static synchronized public String readString(String message) {
		ClientTUIView.print(message);
		String input = "";
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while (input.equals("")) {
			try {				
				input = in.readLine();
				if (in.equals("")) {
					ClientTUIView.print("No command received, please try again");
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return (input == null) ? "" : input;
	}			
}
