package view;

import java.util.Observer;

public interface ClientView extends Observer {
	
	public void showPlayers();
	public void showBoard();
	public void showCurrentPlayer();
	public void showResult();
	public void start();
	public void startUp();
	public void chooseClient();

}
