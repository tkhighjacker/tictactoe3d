package protocol;

/**
 * This interface describes the communication protocol as defined by Lab group 1
 * @author Yannis Linardos
 *
 */


public interface Protocol {
	
	public static final String REGEX = "^[A-Za-z0-9-_]{2,16}$";
	public static final String DILEM = " ";
	
	/*
	 * Below we are going to use the following convention. If an argument is in [] it is mandatory while if it is in <> it is optional
	 */
	
	// Client -> Server
	
	/**
	 * Client enters the lobby. It is followed (mandatory) by the username and (optionally) by additional functionalities.
	 * Example: Join [username] <Chat> <Security> <Challenge> <Leaderboard>
	 */
	public static final String JOIN = "Join %s";
	
	/**
	 * Clients in the lobby use it to start a game. When the required number of players are �ready�, the game will begin.
	 * It is followed (mandatory) by the number of players and (optionally) by the dimension of the game and 
	 * by the noRoof functionality (whether the height of the board will be unlimited)
	 * Example: Ready [number of players] <dimension> <noRoof>
	 */
	public static final String READY = "Ready %d";
	
	
	/**
	 * A client leaves the game. When this happens, all the players go to the lobby. There are no arguments
	 * Example: Leave
	 */
	public static final String LEAVE = "Leave";
	
	
	/**
	 * The player notifies the server of the move he wishes to perform. It is followed by the two coordinates of the move.
	 * Example: Move [x] [z]
	 */
	public static final String MOVE = "Move %d %d";
	
	/**
	 * A client can use this command to disconnect from the server. 
	 * Example: Disconnect
	 */
	public static final String DISCONNECT = "Disconnect";

	
	// Server -> Client
	
	/**
	 * The server notifies the clients of the move that was performed. It is followed by the two coordinates of the move.
	 * Example: NotifyMove [x] [z]
	 */
	public static final String NOTIFYMOVE = "NotifyMove %d %d";
	
	/**
	 * The server notifies the clients that the game has started. It is followed by a
	 * String containing all the usernames of the players separated by space with the order they will play.
	 * Example: StartGame [username1 username2 etc]
	 */
	public static final String STARTGAME = "StartGame %s";
	
	/**
	 * The server notifies the clients that the game is over. It is followed by 
	 * �Draw� if we have a draw, �Winner� + username of the winner if we have a winner
	 * Example: GameOver [Winner username]
	 */
	public static final String GAMEOVER = "GameOver %s";
	
	/**
	 * The server notifies the clients that a player lost connection or left the game. 
	 * In such a case, the game stops and all the other players are thrown back at the lobby. 
	 * It is followed by the username of the player that lost connection or left.
	 * Example: ConnectionLost [username]
	 */
	public static final String CONNECTIONLOST = "ConnectionLost %s";
	
	
	/**
	 * The server notifies the client of an error that occurred. It is followed by 
	 * a message of the error that occurred (i.e. invalid command)
	 * Example: Error [invalid command]
	 */
	public static final String ERROR = "Error %s";
	
	//Below there is the protocol for the optional Bonus assignments
		
	//For the chat functionality
	
	//Client -> Server	
	/**
	 * A client sends the message.
	 * example: Chat [message]
	 */
	public static final String CLIENT_CHAT = "Chat %s";
	
	//Server -> Client
	/**
	 * The server notifies the clients that have the chat functionality that another client sent a message
	 * example: Chat [username (of the client that sent the message)] [message]
	 */
	public static final String SERVER_CHAT = "Chat %s";

	
	//For the leaderboard functionality
	
	//Client -> Server
	/**
	 * A client notifies the server that it wants a leaderboard
	 * example: Leaderboard
	 */
	public static final String CLIENT_LEADERERBOARD = "Leaderboard";
	
	//Server -> Client
	/**
	 * The server replies to a client who asked for a leaderboard
	 * example: Leaderboard [username1] [score (of the username1)] [username2] [(of the username1)] 
	 * where score is the integer number of the games won by the player
	 */
	public static final String SERVER_LEADERBOARD = "Leaderboard %s%d";
	
	//For the security functionality
	
	//Client -> Server
	/**
	 * A client is trying to login or create a new account. 
	 * example: Security [username] [password]
	 * If the username exists, the server checks the password, if it doesn't exist, the server 
	 * creates a new account with the given password
	 */
	public static final String CLIENT_SECURITY = "Security  %s";
	
	//Server -> Client
	/**
	 * A server either verifies or not the login attempt of the client
	 * example: Security [LoginSuccess/LoginDenied] 
	 * depending on whether the attempt to login was successful or not 
	 */
	public static final String SERVER_SECURITY = "Security %s";
	
	//For the challenge functionality
	
	//Client -> Server
	/**
	 * A client asks the server to give him which other players have activated the challenge functionality
	 * Example: GetPlayers
	 */
	public static final String GETPLAYERS = "GetPlayers";
	
	/**
	 * A client sends a challenge request to some other clients who have the challenge functionality activated
	 * example: Challenge [Dimension] [AmountOfPlayers] <NoRoof> [username1] [username2] ...
	 */
	public static final String CHALLENGE = "Challenge %d%s";
	
	/**
	 * A player can either accept or deny the challenge of another player
	 * example: ChallengeAccept [y/n]
	 * Note: if the client does not have challenge functionality or the dimension/amountOfPlayers/noRoof etc 
	 * that the challenge entails are not supported by the client, then it should sent automatically n.
	 */
	public static final String CHALLENGEACCEPT = "ChallengeAccept %s";
	
	//Server -> Client
	/**
	 * The server sends to the player who sent GetPlayers the required information
	 * example: Players [username1] [username2] etc
	 * Note: it should only send the usernames of the players who have challenge functionality
	 */
	public static final String PlAYERS = "Players %s";
	
	/**
	 * The server sends it to a client who have been challenged by another client
	 * example: ChallengeRequest [Dimension] [AmountOfPlayers] <NoRoof> [username (of the player that sent the challenge)]
	 */
	public static final String CHALLENGEREQUEST = "ChallengeRequest %d%s";

}