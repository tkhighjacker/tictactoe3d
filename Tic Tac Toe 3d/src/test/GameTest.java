package test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import controller.Game;
import controller.NormalPlayer;
import controller.Player;
import model.Board;

public class GameTest {
	
	private Board board;
	private Game game;
	private NormalPlayer normalPlayer1;
	private NormalPlayer normalPlayer2;
	private List<Player> players = new ArrayList<>();
	private static final String Tib = "Tib";
	private static final String Tho = "Tho";

	@Before
	public void setUp() throws Exception {
		normalPlayer1 = new NormalPlayer("Tibor");
		normalPlayer2 = new NormalPlayer("Thomas");
		players.add(normalPlayer1);
		players.add(normalPlayer2);
		game = new Game(players);
		board = game.getBoard();
	}
	

	@Test
	public void testGameOverFullBoard() {
		String m = Tib;
		assertFalse(game.gameOver());
		for (int z = 0; z < Board.SIZE; z++) {
			for (int y = 0; y < Board.SIZE; y++) {
				for (int x = 0; x < Board.SIZE; x++) {
					board.setField(x, y, z, m);
				}
			}

		}
		assertTrue(game.gameOver());   
	}
	
	@Test
	public void testHasWinnerRowX() {
		board.setField(0, 0, 0, Tib);
		board.setField(1, 0, 0, Tib);
		board.setField(2, 0, 0, Tib);
		assertFalse(game.hasWinner());

		board.setField(3, 0, 0, Tib);
		assertTrue(game.hasWinner());

	}

	@Test
	public void testHasWinnerRowY() {
		board.setField(0, 0, 0, Tib);
		board.setField(0, 1, 0, Tib);
		board.setField(0, 2, 0, Tib);
		assertFalse(game.hasWinner());

		board.setField(0, 3, 0, Tib);
		assertTrue(game.hasWinner());
	}

	@Test
	public void testHasWinnerRowZ() {
		board.setField(0, 0, 0, Tib);
		board.setField(0, 0, 1, Tib);
		board.setField(0, 0, 2, Tib);
		assertFalse(game.hasWinner());

		board.setField(0, 0, 3, Tib);
		assertTrue(game.hasWinner());
	}

	@Test
	public void testHasWinnerDiagonalXY() {
		board.setField(0, 0, 0, Tib);
		board.setField(1, 1, 0, Tib);
		board.setField(2, 2, 0, Tib);
		assertFalse(game.hasWinner());

		board.setField(3, 3, 0, Tib);
		assertTrue(game.hasWinner());
	}

	@Test
	public void testHasWinnerDiagonalXZ() {
		board.setField(0, 0, 0, Tib); // a tile of the winning diagonal.
		board.setField(0, 1, 0, Tho);
		board.setField(0, 1, 1, Tib); // a tile of the winning diagonal.
		board.setField(0, 2, 0, Tho);
		board.setField(0, 2, 1, Tho);
		board.setField(0, 2, 2, Tib); // a tile of the winning diagonal.
		assertFalse(game.hasWinner());

		board.setField(0, 3, 0, Tho);
		board.setField(0, 3, 1, Tho);
		board.setField(0, 3, 2, Tho);
		board.setField(0, 3, 3, Tib); // a tile of the winning diagonal.
		assertTrue(game.hasWinner());
	}

	@Test
	public void testHasWinnerDiagonalYZ() {
		board.setField(0, 0, 0, Tib); // a tile of the winning diagonal.
		board.setField(1, 0, 0, Tho);
		board.setField(1, 0, 1, Tib); // a tile of the winning diagonal.
		board.setField(2, 0, 0, Tho);
		board.setField(2, 0, 1, Tho);
		board.setField(2, 0, 2, Tib); // a tile of the winning diagonal.
		assertFalse(game.hasWinner());

		board.setField(3, 0, 0, Tho);
		board.setField(3, 0, 1, Tho);
		board.setField(3, 0, 2, Tho);
		board.setField(3, 0, 3, Tib); // a tile of the winning diagonal.
		assertTrue(game.hasWinner());
	}

	@Test
	public void testHasWinnerDiagonalXYZ() {
		board.setField(0, 0, 0, Tib); // a tile of the winning diagonal.
		board.setField(1, 1, 0, Tho);
		board.setField(1, 1, 1, Tib); // a tile of the winning diagonal.
		board.setField(2, 2, 0, Tho);
		board.setField(2, 2, 1, Tho);
		board.setField(2, 2, 2, Tib); // a tile of the winning diagonal.
		assertFalse(game.hasWinner());

		board.setField(3, 3, 0, Tho);
		board.setField(3, 3, 1, Tho);
		board.setField(3, 3, 2, Tho);
		board.setField(3, 3, 3, Tib); // a tile of the winning diagonal.
		assertTrue(game.hasWinner());
	}

}
