package test;

import org.junit.Before;
import org.junit.Test;

import model.Board;
import protocol.Protocol;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BoardTest implements Protocol {
	private Board board;
	private static final String XX = "XX";
	private static final String EM = "";
	private static final String OO = "OO";	

	@Before
	public void setUp() {
		board = new Board();
	}

	@Test
	public void testIndex() {
		assertEquals(0, Board.index(0, 0, 0));
		assertEquals(Board.SIZE * Board.SIZE, Board.index(0, 0, 1));
		assertEquals(1, Board.index(1, 0, 0));
	}

	@Test
	public void testIsFieldIndex() {
		assertFalse(board.isField(-1, -1, -1));
		assertTrue(board.isField(0, 0, 0));
		assertTrue(board.isField(2, 2, 2));
		assertTrue(board.isField(3, 3, 3));
		assertFalse(board.isField(4, 4, 4));
	}

	@Test
	public void testIsFieldRowCol() {
		assertFalse(board.isField(-1, 0, 0));
		assertFalse(board.isField(0, -1, 0));
		assertFalse(board.isField(0, 0, -1));
		assertFalse(board.isField(-1, -1, 0));
		assertFalse(board.isField(0, -1, -1));
		assertFalse(board.isField(-1, -1, -1));
		assertTrue(board.isField(0, 0, 0));
		assertTrue(board.isField(3, 3, 3));
		assertFalse(board.isField(4, 3, 3));
		assertFalse(board.isField(3, 4, 3));
		assertFalse(board.isField(3, 3, 4));
		assertFalse(board.isField(4, 4, 3));
		assertFalse(board.isField(3, 4, 4));
		assertFalse(board.isField(4, 4, 4));
	}

	@Test
	public void testSetAndGetFieldIndex() {
		board.setField(0, 0, 0, XX);
		assertEquals(XX, board.getField(0, 0, 0));
		assertEquals(EM, board.getField(1, 0, 0));
	}

	@Test
	public void testSetFieldRowCol() {
		board.setField(0, 0, 0, XX);
		assertEquals(XX, board.getField(0, 0, 0));
		assertEquals(EM, board.getField(0, 1, 0));
		assertEquals(EM, board.getField(1, 0, 0));
		assertEquals(EM, board.getField(0, 0, 1));
		assertEquals(EM, board.getField(1, 1, 0));
		assertEquals(EM, board.getField(0, 1, 1));
		assertEquals(EM, board.getField(1, 1, 1));
	}

	@Test
	public void testSetup() {
		assertEquals(EM, board.getField(0, 0, 0));
		assertEquals(EM, board.getField(Board.SIZE - 1, Board.SIZE - 1, Board.SIZE - 1));
	}

	@Test
	public void testReset() {
		board.reset();
		assertEquals(EM, board.getField(0, 0, 0));
		assertEquals(EM, board.getField(Board.SIZE - 1, Board.SIZE - 1, Board.SIZE - 1));
	}

	@Test
	public void testDeepCopy() {
		board.setField(0, 0, 0, XX);
		Board deepCopyBoard = board.deepCopy();
		deepCopyBoard.setField(0, 0, 0, OO);

		assertEquals(XX, board.getField(0, 0, 0));
		assertEquals(OO, deepCopyBoard.getField(0, 0, 0));
	}
	
	@Test
	public void testGetFieldNames() {
		board.getFieldNames();
	}
	@Test
	public void testIsEmptyBoard() {
		assertTrue(this.board.isEmptyBoard());
		board.setField(0, 0, 0, XX);
		assertFalse(this.board.isEmptyBoard());
	}

	@Test
	public void testIsEmptyFieldIndex() {
		board.setField(0, 0, 0, XX);
		assertFalse(board.isEmptyField(0, 0, 0));
		assertTrue(board.isEmptyField(1, 0, 0));
	}

	@Test
	public void testIsEmptyFieldRowCol() {
		board.setField(0, 0, 0, XX);
		assertFalse(board.isEmptyField(0, 0, 0));
		assertTrue(board.isEmptyField(0, 1, 0));
		assertTrue(board.isEmptyField(1, 0, 0));
		assertTrue(board.isEmptyField(0, 0, 1));
	}

	@Test
	public void testIsFull() {
		for (int i = 0; i < Board.SIZE; i++) {
			for (int j = 0; j < Board.SIZE; j++) {
				for (int k = 0; k < Board.SIZE; k++) {
					board.setField(i, j, k, XX);
				}
			}
		}
		board.setField(Board.SIZE - 1, Board.SIZE - 1, Board.SIZE - 1, EM);
		assertFalse(board.isFull());
		board.setField(Board.SIZE - 1, Board.SIZE - 1, Board.SIZE - 1, XX);
		assertTrue(board.isFull());
	}

	@Test
	public void testHasRowX() {
		board.setField(0, 0, 0, XX);
		board.setField(1, 0, 0, XX);
		board.setField(2, 0, 0, XX);
		assertFalse(board.hasRowX(XX));
		assertFalse(board.hasRowX(OO));
		board.setField(3, 0, 0, XX);
		assertTrue(board.hasRowX(XX));
		assertFalse(board.hasRow(OO));
	}
	
	public void testHasRowY() {
		board.setField(0, 0, 0, XX);
		board.setField(0, 1, 0, XX);
		board.setField(0, 2, 0, XX);
		assertFalse(board.hasRowY(XX));
		assertFalse(board.hasRowY(OO));
		board.setField(0, 3, 0, XX);
		assertTrue(board.hasRowY(XX));
		assertFalse(board.hasRow(OO));
	}
	
	public void testHasRowZ() {
		board.setField(0, 0, 0, XX);
		board.setField(0, 0, 1, XX);
		board.setField(0, 0, 2, XX);
		assertFalse(board.hasRowZ(XX));
		assertFalse(board.hasRowZ(OO));
		board.setField(0, 0, 3, XX);
		assertTrue(board.hasRowZ(XX));
		assertFalse(board.hasRow(OO));
	}

	@Test
	public void testHasDiagonalUp() {
		board.setField(0, 0, 0, XX);
		board.setField(1, 1, 0, XX);
		board.setField(2, 2, 0, XX);
		assertFalse(board.hasDiagonal(XX));
		assertFalse(board.hasDiagonal(OO));
		
		board.setField(3, 3, 0, XX);
		assertTrue(board.hasDiagonal(XX));
		assertFalse(board.hasDiagonal(OO));
	}

	@Test
	public void testHasDiagonalDown() {
		board.setField(0, 3, 0, XX);
		board.setField(1, 2, 0, XX);
		board.setField(2, 1, 0, XX);
		assertFalse(board.hasDiagonal(XX));
		assertFalse(board.hasDiagonal(OO));

		board.setField(3, 0, 0, XX);
		assertTrue(board.hasDiagonal(XX));
		assertFalse(board.hasDiagonal(OO));
	}
	
	@Test
	public void testHasDiagonalCross() {
		board.setField(0, 0, 0, XX);
		board.setField(1, 1, 1, XX);
		board.setField(2, 2, 2, XX);
		assertFalse(board.hasDiagonal(XX));
		assertFalse(board.hasDiagonal(OO));
		
		board.setField(3, 3, 3, XX);
		assertTrue(board.hasDiagonal(XX));
		assertFalse(board.hasDiagonal(OO));
	}
	
	@Test
	public void testHasDiagonalXY() {
		board.setField(0, 0, 0, XX);
		board.setField(1, 1, 0, XX);
		board.setField(2, 2, 0, XX);
		assertFalse(board.hasDiagonalXY(XX));
		assertFalse(board.hasDiagonalXY(OO));
		
		board.setField(3, 3, 0, XX);
		assertTrue(board.hasDiagonal(XX));
		assertFalse(board.hasDiagonalXY(OO));
	}
	
	@Test
	public void testHasDiagonalXZ() {
		board.setField(0, 0, 0, XX);
		board.setField(1, 0, 1, XX);
		board.setField(2, 0, 2, XX);
		assertFalse(board.hasDiagonalXZ(XX));
		assertFalse(board.hasDiagonalXZ(OO));
		
		board.setField(3, 0, 3, XX);
		assertTrue(board.hasDiagonalXZ(XX));
		assertFalse(board.hasDiagonalXZ(OO));
	}
	
	@Test
	public void testHasDiagonalYZ() {
		board.setField(1, 0, 0, XX);
		board.setField(1, 1, 1, XX);
		board.setField(1, 2, 2, XX);
		assertFalse(board.hasDiagonalYZ(XX));
		assertFalse(board.hasDiagonalYZ(OO));
		
		board.setField(1, 3, 3, XX);
		assertTrue(board.hasDiagonalYZ(XX));
		assertFalse(board.hasDiagonalYZ(OO));		
		
		board.setField(1, 3, 3, XX);
		assertTrue(board.hasDiagonalYZ(XX));
		assertFalse(board.hasDiagonalYZ(OO));
	}
	
	@Test
	public void testHasDiagonalXYZ() {
		board.setField(0, 0, 0, XX);
		board.setField(1, 1, 1, XX);
		board.setField(2, 2, 2, XX);
		assertFalse(board.hasDiagonalYZ(XX));
		assertFalse(board.hasDiagonalYZ(OO));
		
		board.setField(3, 3, 3, XX);
		assertTrue(board.hasDiagonalXYZ(XX));
		assertFalse(board.hasDiagonalXYZ(OO));
	}

	@Test
	public void testIsWinner() {
		board.setField(0, 0, 0, XX);
		board.setField(1, 0, 0, XX);
		board.setField(2, 0, 0, XX);
		assertFalse(board.isWinner(XX));
		assertFalse(board.isWinner(OO));

		board.setField(3, 0, 0, XX);
		assertTrue(board.isWinner(XX));
		assertFalse(board.isWinner(OO));

		board.setField(0, 0, 0, OO);
		board.setField(1, 1, 1, OO);
		board.setField(2, 2, 2, OO);
		assertFalse(board.isWinner(XX));
		assertFalse(board.isWinner(OO));

		board.setField(3, 3, 3, OO);
		assertFalse(board.isWinner(XX));
		assertTrue(board.isWinner(OO));
	}

	

	@Test
	public void testIsValidMove() {
		board.setField(3, 3, 0, OO);
		board.setField(3, 3, 1, XX);
		assertFalse(board.isValidMove(3, 3, 3));
		assertTrue(board.isValidMove(3, 3, 2));
		assertFalse(board.isValidMove(3, 3, 1));
		assertTrue(board.isValidMove(3, 3, 2));
	}

	@Test
	public void testCountRowsContainMarkAtXYZ() {
		assertEquals(0, board.countRowsContainMarkAtXYZ(0, 0, 0, XX));
		assertEquals(13, board.countRowsContainMarkAtXYZ(0, 0, 0, EM));
	}

	@Test
	public void testRowXContainsMark() {
		board.setField(0, 0, 0, XX);
		assertTrue(board.rowXContainsMark(0, 0, XX));
		assertFalse(board.rowXContainsMark(0, 0, OO));
		board.setField(2, 2, 2, XX);
		assertTrue(board.rowXContainsMark(2, 2, XX));
		assertFalse(board.rowXContainsMark(2, 2, OO));
		board.setField(3, 3, 3, XX);
		assertTrue(board.rowXContainsMark(3, 3, XX));
		assertFalse(board.rowXContainsMark(3, 3, OO));
	}

	@Test
	public void testRowYContainsMark() {
		board.setField(0, 0, 0, XX);
		assertTrue(board.rowYContainsMark(0, 0, XX));
		assertFalse(board.rowYContainsMark(0, 0, OO));
		board.setField(2, 2, 2, XX);
		assertTrue(board.rowYContainsMark(2, 2, XX));
		assertFalse(board.rowYContainsMark(2, 2, OO));
		board.setField(3, 3, 3, XX);
		assertTrue(board.rowYContainsMark(3, 3, XX));
		assertFalse(board.rowYContainsMark(3, 3, OO));
	}

	@Test
	public void testRowZContainsMark() {
		board.setField(0, 0, 0, XX);
		assertTrue(board.rowZContainsMark(0, 0, XX));
		assertFalse(board.rowZContainsMark(0, 0, OO));
		board.setField(2, 2, 2, XX);
		assertTrue(board.rowZContainsMark(2, 2, XX));
		assertFalse(board.rowZContainsMark(2, 2, OO));
		board.setField(3, 3, 3, XX);
		assertTrue(board.rowZContainsMark(3, 3, XX));
		assertFalse(board.rowZContainsMark(3, 3, OO));
	}

	@Test
	public void testDiagonalRowDownXYContainsMark() {
		board.setField(0, 0, 0, XX);
		assertTrue(board.diagonalRowDownXYContainsMark(0, 0, 0, XX));
		assertFalse(board.diagonalRowDownXYContainsMark(0, 0, 0, OO));
		board.setField(2, 2, 2, XX);
		assertTrue(board.diagonalRowDownXYContainsMark(2, 2, 2, XX));
		assertFalse(board.diagonalRowDownXYContainsMark(2, 2, 2, OO));
		board.setField(3, 3, 3, XX);
		assertTrue(board.diagonalRowDownXYContainsMark(3, 3, 3, XX));
		assertFalse(board.diagonalRowDownXYContainsMark(3, 3, 3, OO));
	}

	@Test
	public void testDiagonalRowUpXYContainsMark() {
		board.setField(0, 0, 0, XX);
		assertTrue(board.diagonalRowUpXYContainsMark(0, 0, 0, XX));
		assertFalse(board.diagonalRowUpXYContainsMark(0, 0, 0, OO));
		board.setField(2, 2, 2, XX);
		assertTrue(board.diagonalRowUpXYContainsMark(2, 2, 2, XX));
		assertFalse(board.diagonalRowUpXYContainsMark(2, 2, 2, OO));
		board.setField(3, 3, 3, XX);
		assertTrue(board.diagonalRowUpXYContainsMark(3, 3, 3, XX));
		assertFalse(board.diagonalRowUpXYContainsMark(3, 3, 3, OO));
	}

	@Test
	public void testDiagonalRowDownXZContainsMark() {
		board.setField(0, 0, 0, XX);
		assertTrue(board.diagonalRowDownXZContainsMark(0, 0, 0, XX));
		assertFalse(board.diagonalRowDownXZContainsMark(0, 0, 0, OO));
		board.setField(2, 2, 2, XX);
		assertTrue(board.diagonalRowDownXZContainsMark(2, 2, 2, XX));
		assertFalse(board.diagonalRowDownXZContainsMark(2, 2, 2, OO));
		board.setField(3, 3, 3, XX);
		assertTrue(board.diagonalRowDownXZContainsMark(3, 3, 3, XX));
		assertFalse(board.diagonalRowDownXZContainsMark(3, 3, 3, OO));
	}

	@Test
	public void testDiagonalRowUpXZContainsMark() {
		board.setField(0, 0, 0, XX);
		assertTrue(board.diagonalRowUpXZContainsMark(0, 0, 0, XX));
		assertFalse(board.diagonalRowUpXZContainsMark(0, 0, 0, OO));
		board.setField(2, 2, 2, XX);
		assertTrue(board.diagonalRowUpXZContainsMark(2, 2, 2, XX));
		assertFalse(board.diagonalRowUpXZContainsMark(2, 2, 2, OO));
		board.setField(3, 3, 3, XX);
		assertTrue(board.diagonalRowUpXZContainsMark(3, 3, 3, XX));
		assertFalse(board.diagonalRowUpXZContainsMark(3, 3, 3, OO));
	}
	
	@Test
	public void testDiagonalRowDownYZContainsMark() {
		board.setField(0, 0, 0, XX);
		assertTrue(board.diagonalRowUpYZContainsMark(0, 0, 0, XX));
		assertFalse(board.diagonalRowUpYZContainsMark(0, 0, 0, OO));
		board.setField(2, 2, 2, XX);
		assertTrue(board.diagonalRowUpYZContainsMark(2, 2, 2, XX));
		assertFalse(board.diagonalRowUpYZContainsMark(2, 2, 2, OO));
		board.setField(3, 3, 3, XX);
		assertTrue(board.diagonalRowUpYZContainsMark(3, 3, 3, XX));
		assertFalse(board.diagonalRowUpYZContainsMark(3, 3, 3, OO));
	}

	@Test
	public void testDiagonalRowUpYZContainsMark() {
		board.setField(0, 0, 0, XX);
		assertTrue(board.diagonalRowUpYZContainsMark(0, 0, 0, XX));
		assertFalse(board.diagonalRowUpXZContainsMark(0, 0, 0, OO));
		board.setField(2, 2, 2, XX);
		assertTrue(board.diagonalRowUpYZContainsMark(2, 2, 2, XX));
		assertFalse(board.diagonalRowUpYZContainsMark(2, 2, 2, OO));
		board.setField(3, 3, 3, XX);
		assertTrue(board.diagonalRowUpYZContainsMark(3, 3, 3, XX));
		assertFalse(board.diagonalRowUpYZContainsMark(3, 3, 3, OO));
	}

	@Test
	public void testDiagonalRowDownRightXYZContainsMark() {

	}

	@Test
	public void testDiagonalRowDownLeftXYZContainsMark() {

	}

	@Test
	public void testDiagonalRowUpLeftXYZContainsMark() {

	}

	@Test
	public void testDiagonalRowUpRightXYZContainsMark() {

	}
}
