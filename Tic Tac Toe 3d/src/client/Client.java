package client;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.io.BufferedWriter;

import controller.Player;
import protocol.Protocol;
import view.ClientTUIView;
import view.ClientView;


public class Client extends Thread implements Protocol {

	private Socket sock;
	private BufferedWriter out;
	protected Player myPlayer;
	private ServerHandler serverHandler;
	protected boolean isRunning;
	private ClientView view;
	/**
	 * Creates a client by invoking the method startUp().
	 */
	public Client() {
		this.view = new ClientTUIView(this);
		view.startUp();
		try {
			out = new BufferedWriter(new OutputStreamWriter(sock.getOutputStream()));	
			chooseClient();
			serverHandler = new ServerHandler(this, sock);
			serverHandler.start();		
		} catch (IOException e) {
			ClientTUIView.print("Error: Connection refused. Please try again.");
		}
	}
	
	/**
	 * The main method of client.
	 * @param args
	 */
	public static void main(String[] args) {
		Client client = new Client();
		client.start();
	}
	
	/**
	 * Sets the socket of this client.
	 * @param socket to be set as the to be used Socket.
	 */
	public void setSocket(Socket socket) {
		this.sock = socket;
	}
	
	/**
	 * Sets the player of this client.
	 * @param player to be set as myPlayer
	 */
	public void setMyPlayer(Player player) {
		this.myPlayer = player;
	}
	
	/**
	 * This method will keep running as long as the shutdown method isn't called.
	 * During this time it will read the messages from the terminal typed by the user.
	 */
	@Override
	public void run() {
		while (isRunning) {
			String input = ClientTUIView.readString("");
			sendMessage(input);
		}
	}
	
	/**
	 * A method to choose if you want to play as a normal or computer player.
	 * When you enter AI or HUM you will be asked to enter your name.
	 * Afterwards a player will be created with the given name.
	 */
	public void chooseClient() {
		this.view.chooseClient();
		sendMessage(String.format(JOIN, myPlayer.getName()));
	}
	
	/**
	 * @return myPlayer.
	 */
	public Player getMyPlayer() {
		return myPlayer;
	}
	
	/**
	 * Method used to send a message to the server.
	 * @param msg: the message you want to send.
	 */
	public void sendMessage(String msg) {
		try {
			out.write(msg);
			out.newLine();
			out.flush();			
		} catch (IOException e) {
			shutdown();
		}
	}
	
	/** close the socket connection. */
	public void shutdown() {
		ClientTUIView.print("Closing socket connection...");
		try {
			out.close();
			sock.close();
			isRunning = false;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
