package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Scanner;

import controller.NetworkGame;
import controller.Player;
import exceptions.InvalidMoveException;
import protocol.Protocol;
import view.ClientTUIView;

public class ServerHandler extends Thread implements Protocol {
	
	private Client client;
	private Socket sock;
	private BufferedReader in;
	protected NetworkGame clientGame;
	
	private static final String NOTIFYMOVE = "NotifyMove";
	public static final String STARTGAME = "StartGame";
	public static final String GAMEOVER = "GameOver";
	public static final String CONNECTIONLOST = "ConnectionLost";
	public static final String ERROR = "Error";

	/**
	 * The constructor of the ServerHandler.
	 * @param client: the client of the ServerHandler.
	 * @param sock: the Socket of the ServerHandler.
	 */
	public ServerHandler(Client client, Socket sock) {
		this.client = client;
		this.sock = sock;
		try {
			this.in = new BufferedReader(new InputStreamReader(sock.getInputStream()));
		} catch (IOException e) {
			client.shutdown();
		}
		this.client.isRunning = true;		
	}
	
	public void run() {
		try {
			while (client.isRunning) {
				if (in.ready()) {
					String command = in.readLine();
					this.execute(command);
				}
			}
		} catch (IOException e) {
			client.shutdown();
			e.printStackTrace();
		} 
	}
	
	/**
	 * Executes commands given to this method by the server.
	 * When one of the 5 messages is send from server to client 
	 * the corresponding methods will be called and executed.
	 * @param message the message you want to execute.
	 */
	public void execute(String message) {
		Scanner input = new Scanner(message);
		String command = input.next();
		ClientTUIView.print("Server command: " + message);
		switch (command) {
			case NOTIFYMOVE: 
				notifyMove(input);
				break;
			case STARTGAME:
				startGame(input);
				break;
			case GAMEOVER:
				gameOver(input);
				break;
			case CONNECTIONLOST:
				connectionLost(input);
				break;
			case ERROR:
				error(input);
				break;
			default:
				ClientTUIView.print("No command received yet.");
		}
	}
	
	/**
	 * Prints the move of the player before you.
	 * If it is your turn you send a message to the server with your move(x, z).
	 * @param input: the scanner.
	 */
	public void notifyMove(Scanner input) {
		int x = 0;
		int y = 0;
		if (input.hasNextInt()) {
			x = input.nextInt();
		}
		if (input.hasNextInt()) {
			y = input.nextInt();
		}
		Player currentPlayer = clientGame.getCurrentPlayer();
		clientGame.putMark(x, y);
		ClientTUIView.print("Move succesful, the x and y of " + currentPlayer.getName() 
			+ " the move is: " + x + " " + y);
		if (!this.clientGame.getBoard().isWinner(currentPlayer.getAbbreviation())) {
			this.makeMove();
		}
	}
	
	/**
	 * If it's your turn the computerplayer will determine a move on the board of the client.
	 * Afterwards the command MOVE is sent with the the x and z.
	 */
	//@ requires serverHandler.isMyTurn();
	public void makeMove() {
		if (this.isMyTurn()) {
			ClientTUIView.print("It's your turn please make your move");
			int[] move = null;
			try {
				move = client.getMyPlayer().determineMove(this.clientGame.getBoard());
				ClientTUIView.print(this.clientGame.getBoard().toString());
			} catch (InvalidMoveException e) {
				ClientTUIView.printError(e.getMessage());
				ClientTUIView.print("Please choose another field");
				this.makeMove();
			}
			client.sendMessage(String.format(MOVE, move[0], move[1]));
		} else {
			ClientTUIView.print("It's not your turn, please wait your turn");
		}
	}	
	
	/**
	 * Starts a game with the players who joined it.
	 * @param input: the scanner.
	 */
	public void startGame(Scanner input) {
		this.clientGame = new NetworkGame();
		while (input.hasNext()) {
			String playerName = input.next();
			clientGame.addNewClientPlayer(playerName);			
		}
		ClientTUIView.print("Game started with: " + clientGame.getPlayerNames());
		this.makeMove();
	}
	
	/**
	 * Checks if it is your turn.
	 * @return true if it is your turn.
	 */
	public boolean isMyTurn() {
		String myPlayerName = client.getMyPlayer().getName();
		String currentPlayerName = clientGame.getCurrentPlayer().getName();
		return myPlayerName.equals(currentPlayerName);
	}	
	
	public void gameOver(Scanner input) {
		String message = "";
		if (input.hasNext()) {
			message = input.next();
			if (message.equals("Winner")) {
				if (input.hasNext()) {
					String winner = input.next();
					ClientTUIView.print("The winner is " + winner);
				}				
			} else {
				ClientTUIView.print("Draw, there is no winner");
				
			}
			this.removeGame();
		}
	}
	
	/**
	 * Sets the clientGame to null.
	 */
	public void removeGame() {
		this.clientGame = null;
	}
	
	/**
	 * Prints the name of the player who disconnected.
	 * @param input: the scanner.
	 */
	public void connectionLost(Scanner input) {
		if (input.hasNext()) {
			String player = input.next();
			ClientTUIView.print("Player: " + player + " has lost connection");
			this.removeGame();
		}
	}
	
	/**
	 * Prints a errormessage.
	 * @param input: the scanner.
	 */
	public void error(Scanner input) {
		String errorMessage = "";
		if (input.hasNext()) {
			errorMessage = errorMessage + input.next();
		}
		ClientTUIView.print(String.format(ERROR, errorMessage));
	}
	
	/** close the socket connection. */
	public void shutdown() {
		ClientTUIView.print("Closing socket connection...");
		try {
			in.close();
			sock.close();
			client.isRunning = false;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
